CREATE DATABASE  IF NOT EXISTS `wego_rest_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wego_rest_db`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: ec2-50-19-213-178.compute-1.amazonaws.com    Database: wego_rest_db
-- ------------------------------------------------------
-- Server version	5.5.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `binhluan`
--

DROP TABLE IF EXISTS `binhluan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `binhluan` (
  `MaBinhLuan` int(11) NOT NULL AUTO_INCREMENT,
  `MaTaiKhoan` int(11) NOT NULL,
  `MaDuLieu` int(11) DEFAULT NULL,
  `NoiDung` text,
  `THOIGIAN` datetime DEFAULT NULL,
  PRIMARY KEY (`MaBinhLuan`),
  KEY `BINHLUAN_BINHLUAN$MaDuLieu` (`MaDuLieu`),
  KEY `BINHLUAN_BINHLUAN$MaTaiKhoan` (`MaTaiKhoan`),
  CONSTRAINT `BINHLUAN$MaDuLieu` FOREIGN KEY (`MaDuLieu`) REFERENCES `dulieu` (`MaDuLieu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BINHLUAN$MaTaiKhoan` FOREIGN KEY (`MaTaiKhoan`) REFERENCES `taikhoan` (`MaTaiKhoan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `binhluan`
--

LOCK TABLES `binhluan` WRITE;
/*!40000 ALTER TABLE `binhluan` DISABLE KEYS */;
INSERT INTO `binhluan` VALUES (3,13,1,'abc','2013-11-11 15:10:55'),(4,2,1,'Cho 1 like','2013-11-11 17:15:58'),(5,14,2,'hhus','2013-11-11 17:35:42'),(6,14,1,'dftg','2013-11-11 17:37:42'),(7,14,1,'b?','2013-11-11 17:52:29'),(8,14,1,'b?hfb','2013-11-11 17:52:38'),(9,14,1,'hdyh','2013-11-11 18:19:30'),(10,14,1,'hdyh','2013-11-11 18:19:35'),(11,14,1,'hdyh','2013-11-11 18:19:37'),(12,14,1,'yfbhgfft','2013-11-11 19:17:17'),(13,14,1,'yfbhgfft','2013-11-11 19:17:18'),(14,14,1,'fhu','2013-11-11 19:17:42'),(15,14,1,'dgsdgsdg','2013-11-12 09:02:51'),(16,15,1,'test','2013-11-12 15:37:59'),(17,15,1,'hi','2013-11-12 15:49:57'),(18,15,1,'tui','2013-11-12 15:57:05'),(19,15,1,'dang test','2013-11-12 16:04:53'),(20,15,1,'vui','2013-11-12 16:11:35'),(21,15,1,'gh�','2013-11-12 16:14:05'),(22,15,1,'test','2013-11-12 18:28:09'),(23,21,1,'demo','2013-11-12 18:51:24'),(24,15,1,'fasf','2013-11-12 20:40:40'),(25,15,1,'fcyvjhkbk','2013-11-13 05:55:52'),(26,22,2,'cmmmmm','2013-11-13 06:57:58');
/*!40000 ALTER TABLE `binhluan` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_BINHLUAN` AFTER INSERT ON `binhluan`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'BINHLUAN'; 						SET @pk_d = CONCAT('<MaBinhLuan>',NEW.`MaBinhLuan`,'</MaBinhLuan>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_BINHLUAN` AFTER UPDATE ON `binhluan`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'BINHLUAN';						SET @pk_d_old = CONCAT('<MaBinhLuan>',OLD.`MaBinhLuan`,'</MaBinhLuan>');						SET @pk_d = CONCAT('<MaBinhLuan>',NEW.`MaBinhLuan`,'</MaBinhLuan>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_BINHLUAN` AFTER DELETE ON `binhluan`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'BINHLUAN';						SET @pk_d = CONCAT('<MaBinhLuan>',OLD.`MaBinhLuan`,'</MaBinhLuan>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `camxuc_dulieu`
--

DROP TABLE IF EXISTS `camxuc_dulieu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `camxuc_dulieu` (
  `MaDuLieu` int(11) NOT NULL,
  `MaTaiKhoan` int(11) NOT NULL,
  PRIMARY KEY (`MaDuLieu`,`MaTaiKhoan`),
  KEY `FK_MaTaiKhoan_idx` (`MaTaiKhoan`),
  CONSTRAINT `FK_MaDuLieu` FOREIGN KEY (`MaDuLieu`) REFERENCES `dulieu` (`MaDuLieu`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_MaTaiKhoan` FOREIGN KEY (`MaTaiKhoan`) REFERENCES `taikhoan` (`MaTaiKhoan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `camxuc_dulieu`
--

LOCK TABLES `camxuc_dulieu` WRITE;
/*!40000 ALTER TABLE `camxuc_dulieu` DISABLE KEYS */;
/*!40000 ALTER TABLE `camxuc_dulieu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitiet_dulieu`
--

DROP TABLE IF EXISTS `chitiet_dulieu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitiet_dulieu` (
  `MaDuLieu` int(11) NOT NULL DEFAULT '0',
  `MaChiTiet` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MaDuLieu`,`MaChiTiet`),
  KEY `CHITIET_DULIEU_CHITIET_DULIEU$MaChiTiet1` (`MaChiTiet`),
  KEY `CHITIET_DULIEU_CHITIET_DULIEU$MaDuLieu3` (`MaDuLieu`),
  CONSTRAINT `CHITIET_DULIEU$MaChiTiet` FOREIGN KEY (`MaChiTiet`) REFERENCES `chitietdichvu` (`MaChiTiet`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `CHITIET_DULIEU$MaDuLieu1` FOREIGN KEY (`MaDuLieu`) REFERENCES `dulieu` (`MaDuLieu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitiet_dulieu`
--

LOCK TABLES `chitiet_dulieu` WRITE;
/*!40000 ALTER TABLE `chitiet_dulieu` DISABLE KEYS */;
/*!40000 ALTER TABLE `chitiet_dulieu` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_CHITIET_DULIEU` AFTER INSERT ON `chitiet_dulieu`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'CHITIET_DULIEU'; 						SET @pk_d = CONCAT('<MaDuLieu>',NEW.`MaDuLieu`,'</MaDuLieu>','<MaChiTiet>',NEW.`MaChiTiet`,'</MaChiTiet>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_CHITIET_DULIEU` AFTER UPDATE ON `chitiet_dulieu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'CHITIET_DULIEU';						SET @pk_d_old = CONCAT('<MaDuLieu>',OLD.`MaDuLieu`,'</MaDuLieu>','<MaChiTiet>',OLD.`MaChiTiet`,'</MaChiTiet>');						SET @pk_d = CONCAT('<MaDuLieu>',NEW.`MaDuLieu`,'</MaDuLieu>','<MaChiTiet>',NEW.`MaChiTiet`,'</MaChiTiet>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_CHITIET_DULIEU` AFTER DELETE ON `chitiet_dulieu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'CHITIET_DULIEU';						SET @pk_d = CONCAT('<MaDuLieu>',OLD.`MaDuLieu`,'</MaDuLieu>','<MaChiTiet>',OLD.`MaChiTiet`,'</MaChiTiet>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `chitietdichvu`
--

DROP TABLE IF EXISTS `chitietdichvu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitietdichvu` (
  `MaChiTiet` int(11) NOT NULL AUTO_INCREMENT,
  `Ten` varchar(255) DEFAULT '',
  `GiaTien` int(11) DEFAULT NULL,
  `ChuThich` longtext,
  PRIMARY KEY (`MaChiTiet`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitietdichvu`
--

LOCK TABLES `chitietdichvu` WRITE;
/*!40000 ALTER TABLE `chitietdichvu` DISABLE KEYS */;
INSERT INTO `chitietdichvu` VALUES (1,'Phòng đặc biệt (sáng)',40000,NULL),(2,'Phòng đặc biệt (tối)',80000,NULL),(3,'Phòng nhỏ (lớn)',40000,NULL),(4,'Phòng nhỏ (tối)',60000,NULL),(5,'Phòng víp (sáng)',60000,NULL),(6,'Phòng víp (tối)',120000,NULL),(7,'Phòng vip lớn',150000,NULL),(8,'Phòng đặc biệt',100000,NULL),(9,'Phòng thường',60000,NULL),(10,'Phòng víp ',150000,NULL),(11,'Snack',20000,NULL),(12,'Nước suối',20000,NULL),(13,'gà rán',50000,'');
/*!40000 ALTER TABLE `chitietdichvu` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_CHITIETDICHVU` AFTER INSERT ON `chitietdichvu`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'CHITIETDICHVU'; 						SET @pk_d = CONCAT('<MaChiTiet>',NEW.`MaChiTiet`,'</MaChiTiet>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_CHITIETDICHVU` AFTER UPDATE ON `chitietdichvu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'CHITIETDICHVU';						SET @pk_d_old = CONCAT('<MaChiTiet>',OLD.`MaChiTiet`,'</MaChiTiet>');						SET @pk_d = CONCAT('<MaChiTiet>',NEW.`MaChiTiet`,'</MaChiTiet>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_CHITIETDICHVU` AFTER DELETE ON `chitietdichvu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'CHITIETDICHVU';						SET @pk_d = CONCAT('<MaChiTiet>',OLD.`MaChiTiet`,'</MaChiTiet>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dichvu`
--

DROP TABLE IF EXISTS `dichvu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dichvu` (
  `MaDichVu` int(11) NOT NULL AUTO_INCREMENT,
  `TenDichVu` varchar(64) DEFAULT '',
  PRIMARY KEY (`MaDichVu`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dichvu`
--

LOCK TABLES `dichvu` WRITE;
/*!40000 ALTER TABLE `dichvu` DISABLE KEYS */;
INSERT INTO `dichvu` VALUES (1,'Bệnh viện'),(2,'Trường học'),(3,'Quán café'),(4,'Quán ăn'),(5,'ATM'),(6,'Trạm xăng'),(7,'Shop thời trang'),(8,'Tiệm hớt tóc'),(9,'Karaoke'),(10,'Nhà hàng'),(11,'Khách sạn'),(12,'Công viên'),(13,'Chợ'),(14,'Rạp chiếu phim'),(15,'Sân bóng đá'),(16,'Bến xe'),(17,'Phòng khám'),(18,'Bến xe bus'),(19,'Sân vận động'),(20,'Nhà trọ'),(21,'Siêu thị'),(22,'Cơ quan hành chính'),(23,'Cửa hàng kim khí điện máy'),(24,'Cửa hàng đồ gỗ');
/*!40000 ALTER TABLE `dichvu` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_DICHVU` AFTER INSERT ON `dichvu`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DICHVU'; 						SET @pk_d = CONCAT('<MaDichVu>',NEW.`MaDichVu`,'</MaDichVu>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_DICHVU` AFTER UPDATE ON `dichvu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DICHVU';						SET @pk_d_old = CONCAT('<MaDichVu>',OLD.`MaDichVu`,'</MaDichVu>');						SET @pk_d = CONCAT('<MaDichVu>',NEW.`MaDichVu`,'</MaDichVu>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_DICHVU` AFTER DELETE ON `dichvu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DICHVU';						SET @pk_d = CONCAT('<MaDichVu>',OLD.`MaDichVu`,'</MaDichVu>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dulieu`
--

DROP TABLE IF EXISTS `dulieu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dulieu` (
  `MaDuLieu` int(11) NOT NULL AUTO_INCREMENT,
  `MaDichVu` int(11) DEFAULT NULL,
  `MaTenDiaDiem` int(11) DEFAULT NULL,
  `SoNha` varchar(8) DEFAULT '',
  `MaDuong` int(11) DEFAULT NULL,
  `MaPhuong` int(11) DEFAULT NULL,
  `MaQuanHuyen` int(11) DEFAULT NULL,
  `MaTinhThanh` int(11) DEFAULT NULL,
  `KinhDo` double DEFAULT NULL,
  `ViDo` double DEFAULT NULL,
  `ChuThich` longtext,
  `SoLuotThich` int(11) DEFAULT NULL,
  PRIMARY KEY (`MaDuLieu`),
  KEY `DULIEU_DULIEU$KinhDo` (`KinhDo`),
  KEY `DULIEU_DULIEU$MaDichVu1` (`MaDichVu`),
  KEY `DULIEU_DULIEU$MaDichVu2` (`MaDichVu`),
  KEY `DULIEU_DULIEU$MaDuong1` (`MaDuong`),
  KEY `DULIEU_DULIEU$MaDuong2` (`MaDuong`),
  KEY `DULIEU_DULIEU$MaPhuong1` (`MaPhuong`),
  KEY `DULIEU_DULIEU$MaPhuong2` (`MaPhuong`),
  KEY `DULIEU_DULIEU$MaQuanHuyen1` (`MaQuanHuyen`),
  KEY `DULIEU_DULIEU$MaQuanHuyen2` (`MaQuanHuyen`),
  KEY `DULIEU_DULIEU$MaTenDiaDiem1` (`MaTenDiaDiem`),
  KEY `DULIEU_DULIEU$MaTenDiaDiem2` (`MaTenDiaDiem`),
  KEY `DULIEU_DULIEU$MaTinhThanh1` (`MaTinhThanh`),
  KEY `DULIEU_DULIEU$MaTinhThanh2` (`MaTinhThanh`),
  KEY `DULIEU_DULIEU$SoNha` (`SoNha`),
  KEY `DULIEU_DULIEU$ViDo` (`ViDo`),
  CONSTRAINT `DULIEU$MaDichVu` FOREIGN KEY (`MaDichVu`) REFERENCES `dichvu` (`MaDichVu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DULIEU$MaDuong` FOREIGN KEY (`MaDuong`) REFERENCES `duong` (`MaDuong`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DULIEU$MaPhuong` FOREIGN KEY (`MaPhuong`) REFERENCES `phuong` (`MaPhuong`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DULIEU$MaQuanHuyen` FOREIGN KEY (`MaQuanHuyen`) REFERENCES `quanhuyen` (`MaQuanHuyen`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DULIEU$MaTenDiaDiem` FOREIGN KEY (`MaTenDiaDiem`) REFERENCES `tendiadiem` (`MaTenDiaDiem`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DULIEU$MaTinhThanh` FOREIGN KEY (`MaTinhThanh`) REFERENCES `tinhthanh` (`MaTinhThanh`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dulieu`
--

LOCK TABLES `dulieu` WRITE;
/*!40000 ALTER TABLE `dulieu` DISABLE KEYS */;
INSERT INTO `dulieu` VALUES (1,15,76588,'5',1095,220,45,16,106.683658,10.837458,NULL,15),(2,2,76589,'1',1096,220,45,16,106.685418,10.825845,NULL,1),(3,9,76590,'524',1097,220,45,16,106.687843,10.828016,NULL,120),(4,12,76591,'3',1098,223,48,16,106.676577,10.812293,NULL,65),(5,2,76592,'4',1099,222,45,16,106.653167,10.839397,NULL,3),(7,4,76594,'1',1101,224,1,16,106.708445,10.728138,NULL,0),(8,12,76597,'1',1100,214,26,16,106.693508,10.769715,NULL,30),(9,11,76598,'76',1100,225,26,16,106.694303,10.770811,NULL,10),(10,11,76599,'74',1102,225,26,16,106.691472,10.771275,NULL,0),(11,12,76596,'',1105,224,1,16,106.71648,10.725972,NULL,0),(12,2,76600,'90A',1103,226,45,16,106.680762,10.81712,NULL,2),(13,10,76601,'A12',1097,220,45,16,106.685718,10.828585,NULL,45),(14,21,76602,'2',1104,227,45,16,106.679106,10.832463,NULL,66),(15,2,76603,'5',1106,228,45,16,106.687757,10.82163,NULL,12),(16,2,76604,'119',1107,229,45,16,106.662308,10.844834,NULL,45),(17,2,76605,'12',1109,226,45,16,106.687174,10.825719,NULL,65),(18,13,76606,'',1096,226,45,16,106.686984,10.824454,NULL,112),(19,22,76607,'16/1',1104,229,45,16,106.662211,10.837943,NULL,1),(20,1,76608,'212',1108,230,45,16,106.671352,10.854212,NULL,0),(21,9,76609,'81',1108,231,45,16,106.681695,10.836762,NULL,58),(22,5,76610,'305',1116,231,45,16,106.675462,10.839186,NULL,77),(23,2,76611,'',1104,227,45,16,106.66925,10.832379,NULL,3),(24,3,76612,'18',1097,227,45,16,106.667351,10.834971,NULL,102),(25,21,76613,'792',1115,226,45,16,106.679206,10.827658,NULL,115),(26,2,76614,'90',1103,226,45,16,106.680822,10.820155,NULL,3),(27,3,76615,'1',1116,231,45,16,106.675758,10.852863,NULL,33),(28,9,76616,'120',1103,226,45,16,106.682131,10.81889,NULL,153),(29,2,76617,'',1108,230,45,16,106.669149,10.852357,NULL,2),(30,2,76618,'235',1118,228,29,16,106.68177,10.765057,NULL,222),(31,12,76619,'',1119,NULL,26,16,106.691147,10.775828,NULL,120),(32,9,76620,'382',1111,232,1,16,106.710934,10.738885,NULL,0),(33,21,76621,'',1114,224,1,16,106.711159,10.728254,NULL,0),(34,5,76622,'',1114,224,1,16,106.709528,10.729318,NULL,0),(35,2,76623,'19',1110,224,1,16,106.699172,10.731474,NULL,0),(36,21,76624,'',1105,233,1,16,106.718872,10.728997,'Trung tâm thương mại Cresent Mall, gồm các dịch vụ ăn uống, xem phim, giải trí',0);
/*!40000 ALTER TABLE `dulieu` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_DULIEU` AFTER INSERT ON `dulieu`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DULIEU'; 						SET @pk_d = CONCAT('<MaDuLieu>',NEW.`MaDuLieu`,'</MaDuLieu>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_DULIEU` AFTER UPDATE ON `dulieu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DULIEU';						SET @pk_d_old = CONCAT('<MaDuLieu>',OLD.`MaDuLieu`,'</MaDuLieu>');						SET @pk_d = CONCAT('<MaDuLieu>',NEW.`MaDuLieu`,'</MaDuLieu>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_DULIEU` AFTER DELETE ON `dulieu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DULIEU';						SET @pk_d = CONCAT('<MaDuLieu>',OLD.`MaDuLieu`,'</MaDuLieu>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `dulieu_yeuthich`
--

DROP TABLE IF EXISTS `dulieu_yeuthich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dulieu_yeuthich` (
  `MaDuLieu` int(11) NOT NULL,
  `MaTaiKhoan` int(11) NOT NULL,
  PRIMARY KEY (`MaDuLieu`,`MaTaiKhoan`),
  KEY `DULIEU_YEUTHICH$MaTaiKhoan` (`MaTaiKhoan`),
  CONSTRAINT `DULIEU_YEUTHICH$MaDuLieu` FOREIGN KEY (`MaDuLieu`) REFERENCES `dulieu` (`MaDuLieu`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `DULIEU_YEUTHICH$MaTaiKhoan` FOREIGN KEY (`MaTaiKhoan`) REFERENCES `taikhoan` (`MaTaiKhoan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dulieu_yeuthich`
--

LOCK TABLES `dulieu_yeuthich` WRITE;
/*!40000 ALTER TABLE `dulieu_yeuthich` DISABLE KEYS */;
INSERT INTO `dulieu_yeuthich` VALUES (1,2);
/*!40000 ALTER TABLE `dulieu_yeuthich` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_DULIEU_YEUTHICH` AFTER INSERT ON `dulieu_yeuthich`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DULIEU_YEUTHICH'; 						SET @pk_d = CONCAT('<MaDuLieu>',NEW.`MaDuLieu`,'</MaDuLieu>','<MaTaiKhoan>',NEW.`MaTaiKhoan`,'</MaTaiKhoan>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_DULIEU_YEUTHICH` AFTER UPDATE ON `dulieu_yeuthich`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DULIEU_YEUTHICH';						SET @pk_d_old = CONCAT('<MaDuLieu>',OLD.`MaDuLieu`,'</MaDuLieu>','<MaTaiKhoan>',OLD.`MaTaiKhoan`,'</MaTaiKhoan>');						SET @pk_d = CONCAT('<MaDuLieu>',NEW.`MaDuLieu`,'</MaDuLieu>','<MaTaiKhoan>',NEW.`MaTaiKhoan`,'</MaTaiKhoan>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_DULIEU_YEUTHICH` AFTER DELETE ON `dulieu_yeuthich`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DULIEU_YEUTHICH';						SET @pk_d = CONCAT('<MaDuLieu>',OLD.`MaDuLieu`,'</MaDuLieu>','<MaTaiKhoan>',OLD.`MaTaiKhoan`,'</MaTaiKhoan>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `duong`
--

DROP TABLE IF EXISTS `duong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duong` (
  `MaDuong` int(11) NOT NULL AUTO_INCREMENT,
  `TenDuong` varchar(64) DEFAULT '',
  PRIMARY KEY (`MaDuong`),
  UNIQUE KEY `DUONG_DUONG$TenDuong` (`TenDuong`)
) ENGINE=InnoDB AUTO_INCREMENT=1120 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `duong`
--

LOCK TABLES `duong` WRITE;
/*!40000 ALTER TABLE `duong` DISABLE KEYS */;
INSERT INTO `duong` VALUES (1098,'Hoàng Minh Giám'),(1100,'Lê Lai'),(1106,'Lê Lợi'),(1117,'Lê Quang Định'),(1102,'Lê Thị Riêng'),(1112,'Lê Văn Lương'),(1108,'Lê Đức Thọ'),(1084,'Nguyễn An Khương'),(1085,'Nguyễn An Ninh'),(1086,'Nguyễn Ảnh Thủ'),(1087,'Nguyễn Bá Huân'),(1088,'Nguyễn Bá Luật'),(1089,'Nguyễn Biểu'),(1090,'Nguyễn Bình'),(1091,'Nguyễn Bỉnh Khiêm'),(1092,'Nguyễn Cảnh Chân'),(1093,'Nguyễn Chế Nghĩa'),(1094,'Nguyễn Chí Thanh'),(1096,'Nguyễn Du'),(1110,'Nguyễn Hữu Thọ'),(1115,'Nguyễn Kiệm'),(1113,'Nguyễn Lương Bằng'),(1116,'Nguyễn Oanh'),(1103,'Nguyễn Thái Sơn'),(1111,'Nguyễn Thị Thập'),(1109,'Nguyễn Văn Bảo'),(1118,'Nguyễn Văn Cừ'),(1114,'Nguyễn Văn Linh'),(1101,'Nguyễn Đức Cảnh'),(1097,'Phan Văn Trị'),(1104,'Quang Trung'),(1107,'Số 16 - Thống Nhất'),(1105,'Tôn Dật Tiên'),(1119,'Trương Định'),(1095,'Đường 20 - Dương Quảng Hàm'),(1099,'Đường Số 20 - Quang Trung');
/*!40000 ALTER TABLE `duong` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_DUONG` AFTER INSERT ON `duong`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DUONG'; 						SET @pk_d = CONCAT('<MaDuong>',NEW.`MaDuong`,'</MaDuong>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_DUONG` AFTER UPDATE ON `duong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DUONG';						SET @pk_d_old = CONCAT('<MaDuong>',OLD.`MaDuong`,'</MaDuong>');						SET @pk_d = CONCAT('<MaDuong>',NEW.`MaDuong`,'</MaDuong>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_DUONG` AFTER DELETE ON `duong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'DUONG';						SET @pk_d = CONCAT('<MaDuong>',OLD.`MaDuong`,'</MaDuong>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `hinhanh`
--

DROP TABLE IF EXISTS `hinhanh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hinhanh` (
  `MaHinhAnh` int(11) NOT NULL AUTO_INCREMENT,
  `MaDuLieu` int(11) DEFAULT NULL,
  `Url` varchar(64) DEFAULT '',
  PRIMARY KEY (`MaHinhAnh`),
  KEY `HINHANH_HINHANH$MaDuLieu` (`MaDuLieu`),
  CONSTRAINT `HINHANH$MaDuLieu` FOREIGN KEY (`MaDuLieu`) REFERENCES `dulieu` (`MaDuLieu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hinhanh`
--

LOCK TABLES `hinhanh` WRITE;
/*!40000 ALTER TABLE `hinhanh` DISABLE KEYS */;
/*!40000 ALTER TABLE `hinhanh` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_HINHANH` AFTER INSERT ON `hinhanh`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'HINHANH'; 						SET @pk_d = CONCAT('<MaHinhAnh>',NEW.`MaHinhAnh`,'</MaHinhAnh>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_HINHANH` AFTER UPDATE ON `hinhanh`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'HINHANH';						SET @pk_d_old = CONCAT('<MaHinhAnh>',OLD.`MaHinhAnh`,'</MaHinhAnh>');						SET @pk_d = CONCAT('<MaHinhAnh>',NEW.`MaHinhAnh`,'</MaHinhAnh>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_HINHANH` AFTER DELETE ON `hinhanh`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'HINHANH';						SET @pk_d = CONCAT('<MaHinhAnh>',OLD.`MaHinhAnh`,'</MaHinhAnh>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `history_store`
--

DROP TABLE IF EXISTS `history_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_store` (
  `timemark` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `table_name` tinytext NOT NULL,
  `pk_date_src` text NOT NULL,
  `pk_date_dest` text NOT NULL,
  `record_state` int(11) NOT NULL,
  PRIMARY KEY (`table_name`(100),`pk_date_dest`(100))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_store`
--

LOCK TABLES `history_store` WRITE;
/*!40000 ALTER TABLE `history_store` DISABLE KEYS */;
INSERT INTO `history_store` VALUES ('2013-10-27 14:32:11','CHITIETDICHVU','<MaChiTiet>1</MaChiTiet>','<MaChiTiet>1</MaChiTiet>',1),('2013-10-27 14:33:43','CHITIETDICHVU','<MaChiTiet>2</MaChiTiet>','<MaChiTiet>2</MaChiTiet>',1),('2013-10-27 14:34:47','CHITIETDICHVU','<MaChiTiet>3</MaChiTiet>','<MaChiTiet>3</MaChiTiet>',1),('2013-10-27 14:35:39','CHITIETDICHVU','<MaChiTiet>4</MaChiTiet>','<MaChiTiet>4</MaChiTiet>',1),('2013-10-27 14:35:39','CHITIETDICHVU','<MaChiTiet>5</MaChiTiet>','<MaChiTiet>5</MaChiTiet>',1),('2013-10-27 14:35:40','CHITIETDICHVU','<MaChiTiet>6</MaChiTiet>','<MaChiTiet>6</MaChiTiet>',1),('2013-10-27 14:35:40','CHITIETDICHVU','<MaChiTiet>7</MaChiTiet>','<MaChiTiet>7</MaChiTiet>',1),('2013-10-27 14:35:40','CHITIETDICHVU','<MaChiTiet>8</MaChiTiet>','<MaChiTiet>8</MaChiTiet>',1),('2013-10-27 14:35:40','CHITIETDICHVU','<MaChiTiet>9</MaChiTiet>','<MaChiTiet>9</MaChiTiet>',1),('2013-10-27 14:35:40','CHITIETDICHVU','<MaChiTiet>10</MaChiTiet>','<MaChiTiet>10</MaChiTiet>',1),('2013-10-27 14:35:40','CHITIETDICHVU','<MaChiTiet>11</MaChiTiet>','<MaChiTiet>11</MaChiTiet>',1),('2013-10-27 14:35:40','CHITIETDICHVU','<MaChiTiet>12</MaChiTiet>','<MaChiTiet>12</MaChiTiet>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>1</MaDichVu>','<MaDichVu>1</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>2</MaDichVu>','<MaDichVu>2</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>3</MaDichVu>','<MaDichVu>3</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>4</MaDichVu>','<MaDichVu>4</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>5</MaDichVu>','<MaDichVu>5</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>6</MaDichVu>','<MaDichVu>6</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>7</MaDichVu>','<MaDichVu>7</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>8</MaDichVu>','<MaDichVu>8</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>9</MaDichVu>','<MaDichVu>9</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>10</MaDichVu>','<MaDichVu>10</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>11</MaDichVu>','<MaDichVu>11</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>12</MaDichVu>','<MaDichVu>12</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>13</MaDichVu>','<MaDichVu>13</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>14</MaDichVu>','<MaDichVu>14</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>15</MaDichVu>','<MaDichVu>15</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>16</MaDichVu>','<MaDichVu>16</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>17</MaDichVu>','<MaDichVu>17</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>18</MaDichVu>','<MaDichVu>18</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>19</MaDichVu>','<MaDichVu>19</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>20</MaDichVu>','<MaDichVu>20</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>21</MaDichVu>','<MaDichVu>21</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>22</MaDichVu>','<MaDichVu>22</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>23</MaDichVu>','<MaDichVu>23</MaDichVu>',1),('2013-10-27 14:36:19','DICHVU','<MaDichVu>24</MaDichVu>','<MaDichVu>24</MaDichVu>',1),('2013-10-27 14:37:08','DUONG','<MaDuong>1084</MaDuong>','<MaDuong>1084</MaDuong>',1),('2013-10-27 14:37:08','DUONG','<MaDuong>1085</MaDuong>','<MaDuong>1085</MaDuong>',1),('2013-10-27 14:37:08','DUONG','<MaDuong>1086</MaDuong>','<MaDuong>1086</MaDuong>',1),('2013-10-27 14:37:08','DUONG','<MaDuong>1087</MaDuong>','<MaDuong>1087</MaDuong>',1),('2013-10-27 14:37:08','DUONG','<MaDuong>1088</MaDuong>','<MaDuong>1088</MaDuong>',1),('2013-10-27 14:37:08','DUONG','<MaDuong>1089</MaDuong>','<MaDuong>1089</MaDuong>',1),('2013-10-27 14:37:08','DUONG','<MaDuong>1090</MaDuong>','<MaDuong>1090</MaDuong>',1),('2013-10-27 14:37:09','DUONG','<MaDuong>1091</MaDuong>','<MaDuong>1091</MaDuong>',1),('2013-10-27 14:37:09','DUONG','<MaDuong>1092</MaDuong>','<MaDuong>1092</MaDuong>',1),('2013-10-27 14:37:09','DUONG','<MaDuong>1093</MaDuong>','<MaDuong>1093</MaDuong>',1),('2013-10-27 14:37:09','DUONG','<MaDuong>1094</MaDuong>','<MaDuong>1094</MaDuong>',1),('2013-10-27 14:37:41','PHUONG','<MaPhuong>210</MaPhuong>','<MaPhuong>210</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>211</MaPhuong>','<MaPhuong>211</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>212</MaPhuong>','<MaPhuong>212</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>213</MaPhuong>','<MaPhuong>213</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>214</MaPhuong>','<MaPhuong>214</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>215</MaPhuong>','<MaPhuong>215</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>216</MaPhuong>','<MaPhuong>216</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>217</MaPhuong>','<MaPhuong>217</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>218</MaPhuong>','<MaPhuong>218</MaPhuong>',1),('2013-10-27 14:37:42','PHUONG','<MaPhuong>219</MaPhuong>','<MaPhuong>219</MaPhuong>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>26</MaQuanHuyen>','<MaQuanHuyen>26</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>27</MaQuanHuyen>','<MaQuanHuyen>27</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>28</MaQuanHuyen>','<MaQuanHuyen>28</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>29</MaQuanHuyen>','<MaQuanHuyen>29</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>30</MaQuanHuyen>','<MaQuanHuyen>30</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>31</MaQuanHuyen>','<MaQuanHuyen>31</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>32</MaQuanHuyen>','<MaQuanHuyen>32</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>33</MaQuanHuyen>','<MaQuanHuyen>33</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>34</MaQuanHuyen>','<MaQuanHuyen>34</MaQuanHuyen>',1),('2013-10-27 14:38:33','QUANHUYEN','<MaQuanHuyen>44</MaQuanHuyen>','<MaQuanHuyen>44</MaQuanHuyen>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76576</MaTenDiaDiem>','<MaTenDiaDiem>76576</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76577</MaTenDiaDiem>','<MaTenDiaDiem>76577</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76578</MaTenDiaDiem>','<MaTenDiaDiem>76578</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76579</MaTenDiaDiem>','<MaTenDiaDiem>76579</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76580</MaTenDiaDiem>','<MaTenDiaDiem>76580</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76581</MaTenDiaDiem>','<MaTenDiaDiem>76581</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76582</MaTenDiaDiem>','<MaTenDiaDiem>76582</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76583</MaTenDiaDiem>','<MaTenDiaDiem>76583</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76584</MaTenDiaDiem>','<MaTenDiaDiem>76584</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76585</MaTenDiaDiem>','<MaTenDiaDiem>76585</MaTenDiaDiem>',1),('2013-10-27 14:40:06','TENDIADIEM','<MaTenDiaDiem>76586</MaTenDiaDiem>','<MaTenDiaDiem>76586</MaTenDiaDiem>',1),('2013-10-27 14:40:07','TENDIADIEM','<MaTenDiaDiem>76587</MaTenDiaDiem>','<MaTenDiaDiem>76587</MaTenDiaDiem>',1),('2013-10-27 14:40:27','TINHTHANH','<MaTinhThanh>16</MaTinhThanh>','<MaTinhThanh>16</MaTinhThanh>',1),('2013-11-03 15:05:26','QUANHUYEN','<MaQuanHuyen>45</MaQuanHuyen>','<MaQuanHuyen>45</MaQuanHuyen>',1),('2013-11-03 15:05:56','QUANHUYEN','<MaQuanHuyen>46</MaQuanHuyen>','<MaQuanHuyen>46</MaQuanHuyen>',1),('2013-11-03 15:06:05','QUANHUYEN','<MaQuanHuyen>47</MaQuanHuyen>','<MaQuanHuyen>47</MaQuanHuyen>',1),('2013-11-03 15:10:01','QUANHUYEN','<MaQuanHuyen>1</MaQuanHuyen>','<MaQuanHuyen>1</MaQuanHuyen>',1),('2013-11-06 14:08:41','TAIKHOAN','<MaTaiKhoan>1</MaTaiKhoan>','<MaTaiKhoan>1</MaTaiKhoan>',1),('2013-11-16 16:34:58','TAIKHOAN','<MaTaiKhoan>2</MaTaiKhoan>','<MaTaiKhoan>2</MaTaiKhoan>',1),('2013-11-06 14:55:49','TAIKHOAN','<MaTaiKhoan>3</MaTaiKhoan>','<MaTaiKhoan>3</MaTaiKhoan>',1),('2013-11-06 15:01:05','TAIKHOAN','<MaTaiKhoan>4</MaTaiKhoan>','<MaTaiKhoan>4</MaTaiKhoan>',1),('2013-11-06 15:35:10','TAIKHOAN','<MaTaiKhoan>5</MaTaiKhoan>','<MaTaiKhoan>5</MaTaiKhoan>',1),('2013-11-08 18:08:24','TAIKHOAN','<MaTaiKhoan>6</MaTaiKhoan>','<MaTaiKhoan>6</MaTaiKhoan>',1),('2013-11-08 18:09:12','TAIKHOAN','<MaTaiKhoan>7</MaTaiKhoan>','<MaTaiKhoan>7</MaTaiKhoan>',1),('2013-11-08 18:09:45','TAIKHOAN','<MaTaiKhoan>8</MaTaiKhoan>','<MaTaiKhoan>8</MaTaiKhoan>',1),('2013-11-08 18:10:14','TAIKHOAN','<MaTaiKhoan>10</MaTaiKhoan>','<MaTaiKhoan>10</MaTaiKhoan>',1),('2013-11-08 18:42:12','TAIKHOAN','<MaTaiKhoan>11</MaTaiKhoan>','<MaTaiKhoan>11</MaTaiKhoan>',1),('2013-11-09 07:28:52','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>1</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>1</MaTuKhoaQuanHuyen>',1),('2013-11-09 07:28:52','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>2</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>2</MaTuKhoaQuanHuyen>',1),('2013-11-09 10:31:12','TAIKHOAN','<MaTaiKhoan>12</MaTaiKhoan>','<MaTaiKhoan>12</MaTaiKhoan>',1),('2013-11-09 10:34:38','TAIKHOAN','<MaTaiKhoan>13</MaTaiKhoan>','<MaTaiKhoan>13</MaTaiKhoan>',1),('2013-11-09 11:28:42','TAIKHOAN','<MaTaiKhoan>14</MaTaiKhoan>','<MaTaiKhoan>14</MaTaiKhoan>',1),('2013-11-09 20:38:45','TAIKHOAN','<MaTaiKhoan>15</MaTaiKhoan>','<MaTaiKhoan>15</MaTaiKhoan>',1),('2013-11-09 20:55:38','TAIKHOAN','<MaTaiKhoan>16</MaTaiKhoan>','<MaTaiKhoan>16</MaTaiKhoan>',1),('2013-11-09 20:58:14','TAIKHOAN','<MaTaiKhoan>17</MaTaiKhoan>','<MaTaiKhoan>17</MaTaiKhoan>',1),('2013-11-10 06:39:52','PHUONG','<MaPhuong>220</MaPhuong>','<MaPhuong>220</MaPhuong>',1),('2013-11-10 06:40:02','DUONG','<MaDuong>1095</MaDuong>','<MaDuong>1095</MaDuong>',1),('2013-11-10 06:41:56','TENDIADIEM','<MaTenDiaDiem>76588</MaTenDiaDiem>','<MaTenDiaDiem>76588</MaTenDiaDiem>',1),('2013-11-17 06:35:27','DULIEU','<MaDuLieu>1</MaDuLieu>','<MaDuLieu>1</MaDuLieu>',1),('2013-11-10 07:08:46','TENDIADIEM','<MaTenDiaDiem>76589</MaTenDiaDiem>','<MaTenDiaDiem>76589</MaTenDiaDiem>',1),('2013-11-10 07:09:54','DUONG','<MaDuong>1096</MaDuong>','<MaDuong>1096</MaDuong>',1),('2013-11-10 07:10:15','PHUONG','<MaPhuong>221</MaPhuong>','<MaPhuong>221</MaPhuong>',1),('2013-11-10 07:16:31','DULIEU','<MaDuLieu>2</MaDuLieu>','<MaDuLieu>2</MaDuLieu>',1),('2013-11-10 07:17:49','TUKHOADICHVU','<MaTuKhoaDichVu>1</MaTuKhoaDichVu>','<MaTuKhoaDichVu>1</MaTuKhoaDichVu>',1),('2013-11-10 07:17:50','TUKHOADICHVU','<MaTuKhoaDichVu>2</MaTuKhoaDichVu>','<MaTuKhoaDichVu>2</MaTuKhoaDichVu>',1),('2013-11-10 07:17:51','TUKHOADICHVU','<MaTuKhoaDichVu>3</MaTuKhoaDichVu>','<MaTuKhoaDichVu>3</MaTuKhoaDichVu>',1),('2013-11-10 07:17:51','TUKHOADICHVU','<MaTuKhoaDichVu>4</MaTuKhoaDichVu>','<MaTuKhoaDichVu>4</MaTuKhoaDichVu>',1),('2013-11-10 07:17:52','TUKHOADICHVU','<MaTuKhoaDichVu>5</MaTuKhoaDichVu>','<MaTuKhoaDichVu>5</MaTuKhoaDichVu>',1),('2013-11-10 07:18:37','TUKHOADUONG','<MaTuKhoaDuong>1</MaTuKhoaDuong>','<MaTuKhoaDuong>1</MaTuKhoaDuong>',1),('2013-11-10 07:18:37','TUKHOADUONG','<MaTuKhoaDuong>2</MaTuKhoaDuong>','<MaTuKhoaDuong>2</MaTuKhoaDuong>',1),('2013-11-10 07:18:38','TUKHOADUONG','<MaTuKhoaDuong>3</MaTuKhoaDuong>','<MaTuKhoaDuong>3</MaTuKhoaDuong>',1),('2013-11-10 07:20:08','TUKHOADUONG','<MaTuKhoaDuong>4</MaTuKhoaDuong>','<MaTuKhoaDuong>4</MaTuKhoaDuong>',1),('2013-11-10 07:22:08','TUKHOADUONG','<MaTuKhoaDuong>5</MaTuKhoaDuong>','<MaTuKhoaDuong>5</MaTuKhoaDuong>',1),('2013-11-10 07:23:11','TUKHOAPHUONG','<MaTuKhoaPhuong>1</MaTuKhoaPhuong>','<MaTuKhoaPhuong>1</MaTuKhoaPhuong>',1),('2013-11-10 07:24:30','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>3</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>3</MaTuKhoaQuanHuyen>',1),('2013-11-10 07:24:31','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>4</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>4</MaTuKhoaQuanHuyen>',1),('2013-11-10 07:26:57','DUONG','<MaDuong>1097</MaDuong>','<MaDuong>1097</MaDuong>',1),('2013-11-10 07:27:25','TUKHOADICHVU','<MaTuKhoaDichVu>6</MaTuKhoaDichVu>','<MaTuKhoaDichVu>6</MaTuKhoaDichVu>',1),('2013-11-10 07:30:28','DULIEU','<MaDuLieu>3</MaDuLieu>','<MaDuLieu>3</MaDuLieu>',1),('2013-11-10 07:30:13','TENDIADIEM','<MaTenDiaDiem>76590</MaTenDiaDiem>','<MaTenDiaDiem>76590</MaTenDiaDiem>',1),('2013-11-10 07:33:44','TENDIADIEM','<MaTenDiaDiem>76591</MaTenDiaDiem>','<MaTenDiaDiem>76591</MaTenDiaDiem>',1),('2013-11-10 07:34:44','QUANHUYEN','<MaQuanHuyen>48</MaQuanHuyen>','<MaQuanHuyen>48</MaQuanHuyen>',1),('2013-11-10 07:35:10','PHUONG','<MaPhuong>222</MaPhuong>','<MaPhuong>222</MaPhuong>',1),('2013-11-10 07:35:10','PHUONG','<MaPhuong>223</MaPhuong>','<MaPhuong>223</MaPhuong>',1),('2013-11-10 07:35:39','DUONG','<MaDuong>1098</MaDuong>','<MaDuong>1098</MaDuong>',1),('2013-11-10 07:36:09','DULIEU','<MaDuLieu>4</MaDuLieu>','<MaDuLieu>4</MaDuLieu>',1),('2013-11-10 07:36:37','TUKHOADICHVU','<MaTuKhoaDichVu>7</MaTuKhoaDichVu>','<MaTuKhoaDichVu>7</MaTuKhoaDichVu>',1),('2013-11-10 07:41:17','TUKHOADUONG','<MaTuKhoaDuong>6</MaTuKhoaDuong>','<MaTuKhoaDuong>6</MaTuKhoaDuong>',1),('2013-11-10 07:41:17','TUKHOADUONG','<MaTuKhoaDuong>7</MaTuKhoaDuong>','<MaTuKhoaDuong>7</MaTuKhoaDuong>',1),('2013-11-10 07:37:20','TUKHOAPHUONG','<MaTuKhoaPhuong>2</MaTuKhoaPhuong>','<MaTuKhoaPhuong>2</MaTuKhoaPhuong>',1),('2013-11-10 07:37:20','TUKHOAPHUONG','<MaTuKhoaPhuong>3</MaTuKhoaPhuong>','<MaTuKhoaPhuong>3</MaTuKhoaPhuong>',1),('2013-11-10 07:37:58','TENDIADIEM','<MaTenDiaDiem>76592</MaTenDiaDiem>','<MaTenDiaDiem>76592</MaTenDiaDiem>',1),('2013-11-10 07:39:18','DUONG','<MaDuong>1099</MaDuong>','<MaDuong>1099</MaDuong>',1),('2013-11-10 07:40:23','DULIEU','<MaDuLieu>5</MaDuLieu>','<MaDuLieu>5</MaDuLieu>',1),('2013-11-10 07:41:16','TUKHOADUONG','<MaTuKhoaDuong>8</MaTuKhoaDuong>','<MaTuKhoaDuong>8</MaTuKhoaDuong>',1),('2013-11-10 07:41:51','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>5</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>5</MaTuKhoaQuanHuyen>',1),('2013-11-10 07:41:52','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>6</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>6</MaTuKhoaQuanHuyen>',1),('2013-11-10 07:41:52','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>7</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>7</MaTuKhoaQuanHuyen>',1),('2013-11-10 10:02:34','TENDIADIEM','<MaTenDiaDiem>76594</MaTenDiaDiem>','<MaTenDiaDiem>76594</MaTenDiaDiem>',1),('2013-11-10 10:04:35','TENDIADIEM','<MaTenDiaDiem>76595</MaTenDiaDiem>','<MaTenDiaDiem>76595</MaTenDiaDiem>',1),('2013-11-10 10:04:36','TENDIADIEM','<MaTenDiaDiem>76596</MaTenDiaDiem>','<MaTenDiaDiem>76596</MaTenDiaDiem>',1),('2013-11-10 10:34:22','QUANHUYEN','<MaQuanHuyen>49</MaQuanHuyen>','<MaQuanHuyen>49</MaQuanHuyen>',1),('2013-11-10 10:44:42','DUONG','<MaDuong>1100</MaDuong>','<MaDuong>1100</MaDuong>',1),('2013-11-10 10:44:47','CHITIETDICHVU','<MaChiTiet>13</MaChiTiet>','<MaChiTiet>13</MaChiTiet>',1),('2013-11-10 17:33:56','DULIEU','<MaDuLieu>34</MaDuLieu>','<MaDuLieu>34</MaDuLieu>',1),('2013-11-10 10:49:18','TENDIADIEM','<MaTenDiaDiem>76597</MaTenDiaDiem>','<MaTenDiaDiem>76597</MaTenDiaDiem>',1),('2013-11-10 10:49:32','DUONG','<MaDuong>1101</MaDuong>','<MaDuong>1101</MaDuong>',1),('2013-11-10 10:51:51','PHUONG','<MaPhuong>224</MaPhuong>','<MaPhuong>224</MaPhuong>',1),('2013-11-10 12:17:22','DUONG','<MaDuong>1103</MaDuong>','<MaDuong>1103</MaDuong>',1),('2013-11-10 10:54:12','DULIEU','<MaDuLieu>7</MaDuLieu>','<MaDuLieu>7</MaDuLieu>',1),('2013-11-10 10:56:16','DULIEU','<MaDuLieu>8</MaDuLieu>','<MaDuLieu>8</MaDuLieu>',1),('2013-11-10 10:56:16','TUKHOADICHVU','<MaTuKhoaDichVu>8</MaTuKhoaDichVu>','<MaTuKhoaDichVu>8</MaTuKhoaDichVu>',1),('2013-11-10 16:42:09','DULIEU','<MaDuLieu>32</MaDuLieu>','<MaDuLieu>32</MaDuLieu>',1),('2013-11-10 10:58:53','TENDIADIEM','<MaTenDiaDiem>76598</MaTenDiaDiem>','<MaTenDiaDiem>76598</MaTenDiaDiem>',1),('2013-11-10 12:34:47','PHUONG','<MaPhuong>227</MaPhuong>','<MaPhuong>227</MaPhuong>',1),('2013-11-10 11:02:29','PHUONG','<MaPhuong>225</MaPhuong>','<MaPhuong>225</MaPhuong>',1),('2013-11-10 11:05:10','DULIEU','<MaDuLieu>9</MaDuLieu>','<MaDuLieu>9</MaDuLieu>',1),('2013-11-10 11:07:11','TENDIADIEM','<MaTenDiaDiem>76599</MaTenDiaDiem>','<MaTenDiaDiem>76599</MaTenDiaDiem>',1),('2013-11-10 11:07:49','DUONG','<MaDuong>1102</MaDuong>','<MaDuong>1102</MaDuong>',1),('2013-11-10 11:10:28','DULIEU','<MaDuLieu>10</MaDuLieu>','<MaDuLieu>10</MaDuLieu>',1),('2013-11-10 11:15:42','TAIKHOAN','<MaTaiKhoan>18</MaTaiKhoan>','<MaTaiKhoan>18</MaTaiKhoan>',1),('2013-11-10 12:07:14','DUONG','<MaDuong>1105</MaDuong>','<MaDuong>1105</MaDuong>',1),('2013-11-10 12:08:55','DULIEU','<MaDuLieu>11</MaDuLieu>','<MaDuLieu>11</MaDuLieu>',1),('2013-11-10 12:18:08','PHUONG','<MaPhuong>226</MaPhuong>','<MaPhuong>226</MaPhuong>',1),('2013-11-10 12:19:44','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>8</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>8</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:19:45','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>9</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>9</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:19:45','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>10</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>10</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:19:46','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>11</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>11</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:20:46','TENDIADIEM','<MaTenDiaDiem>76600</MaTenDiaDiem>','<MaTenDiaDiem>76600</MaTenDiaDiem>',1),('2013-11-10 12:20:54','DULIEU','<MaDuLieu>12</MaDuLieu>','<MaDuLieu>12</MaDuLieu>',1),('2013-11-10 12:27:05','TENDIADIEM','<MaTenDiaDiem>76601</MaTenDiaDiem>','<MaTenDiaDiem>76601</MaTenDiaDiem>',1),('2013-11-10 12:27:50','DULIEU','<MaDuLieu>13</MaDuLieu>','<MaDuLieu>13</MaDuLieu>',1),('2013-11-10 12:30:09','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>12</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>12</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:30:09','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>13</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>13</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:30:10','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>14</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>14</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:31:56','TUKHOADIADIEM','<MaTuKhoaTenDiaDiem>5</MaTuKhoaTenDiaDiem>','<MaTuKhoaTenDiaDiem>5</MaTuKhoaTenDiaDiem>',1),('2013-11-10 12:31:56','TUKHOADIADIEM','<MaTuKhoaTenDiaDiem>4</MaTuKhoaTenDiaDiem>','<MaTuKhoaTenDiaDiem>4</MaTuKhoaTenDiaDiem>',1),('2013-11-10 12:31:55','TUKHOADIADIEM','<MaTuKhoaTenDiaDiem>3</MaTuKhoaTenDiaDiem>','<MaTuKhoaTenDiaDiem>3</MaTuKhoaTenDiaDiem>',1),('2013-11-10 12:31:55','TUKHOADIADIEM','<MaTuKhoaTenDiaDiem>2</MaTuKhoaTenDiaDiem>','<MaTuKhoaTenDiaDiem>2</MaTuKhoaTenDiaDiem>',1),('2013-11-10 12:31:54','TUKHOADIADIEM','<MaTuKhoaTenDiaDiem>1</MaTuKhoaTenDiaDiem>','<MaTuKhoaTenDiaDiem>1</MaTuKhoaTenDiaDiem>',1),('2013-11-10 12:31:57','TUKHOADIADIEM','<MaTuKhoaTenDiaDiem>7</MaTuKhoaTenDiaDiem>','<MaTuKhoaTenDiaDiem>7</MaTuKhoaTenDiaDiem>',1),('2013-11-10 12:30:32','TUKHOADICHVU','<MaTuKhoaDichVu>10</MaTuKhoaDichVu>','<MaTuKhoaDichVu>10</MaTuKhoaDichVu>',1),('2013-11-10 12:30:32','TUKHOADICHVU','<MaTuKhoaDichVu>11</MaTuKhoaDichVu>','<MaTuKhoaDichVu>11</MaTuKhoaDichVu>',1),('2013-11-10 12:30:33','TUKHOADICHVU','<MaTuKhoaDichVu>12</MaTuKhoaDichVu>','<MaTuKhoaDichVu>12</MaTuKhoaDichVu>',1),('2013-11-10 12:30:33','TUKHOADICHVU','<MaTuKhoaDichVu>13</MaTuKhoaDichVu>','<MaTuKhoaDichVu>13</MaTuKhoaDichVu>',1),('2013-11-10 16:42:10','DULIEU','<MaDuLieu>33</MaDuLieu>','<MaDuLieu>33</MaDuLieu>',1),('2013-11-10 12:30:34','TUKHOADICHVU','<MaTuKhoaDichVu>15</MaTuKhoaDichVu>','<MaTuKhoaDichVu>15</MaTuKhoaDichVu>',1),('2013-11-10 12:30:35','TUKHOADICHVU','<MaTuKhoaDichVu>16</MaTuKhoaDichVu>','<MaTuKhoaDichVu>16</MaTuKhoaDichVu>',1),('2013-11-10 17:01:32','TENDIADIEM','<MaTenDiaDiem>76622</MaTenDiaDiem>','<MaTenDiaDiem>76622</MaTenDiaDiem>',1),('2013-11-10 12:30:36','TUKHOADICHVU','<MaTuKhoaDichVu>18</MaTuKhoaDichVu>','<MaTuKhoaDichVu>18</MaTuKhoaDichVu>',1),('2013-11-10 12:31:57','TUKHOADIADIEM','<MaTuKhoaTenDiaDiem>6</MaTuKhoaTenDiaDiem>','<MaTuKhoaTenDiaDiem>6</MaTuKhoaTenDiaDiem>',1),('2013-11-10 12:32:49','TUKHOADUONG','<MaTuKhoaDuong>9</MaTuKhoaDuong>','<MaTuKhoaDuong>9</MaTuKhoaDuong>',1),('2013-11-10 12:33:00','TUKHOADUONG','<MaTuKhoaDuong>10</MaTuKhoaDuong>','<MaTuKhoaDuong>10</MaTuKhoaDuong>',1),('2013-11-10 12:33:51','DUONG','<MaDuong>1104</MaDuong>','<MaDuong>1104</MaDuong>',1),('2013-11-10 12:34:01','TENDIADIEM','<MaTenDiaDiem>76602</MaTenDiaDiem>','<MaTenDiaDiem>76602</MaTenDiaDiem>',1),('2013-11-10 12:35:22','DULIEU','<MaDuLieu>14</MaDuLieu>','<MaDuLieu>14</MaDuLieu>',1),('2013-11-10 12:35:31','TUKHOAPHUONG','<MaTuKhoaPhuong>4</MaTuKhoaPhuong>','<MaTuKhoaPhuong>4</MaTuKhoaPhuong>',1),('2013-11-10 12:35:44','TUKHOAPHUONG','<MaTuKhoaPhuong>5</MaTuKhoaPhuong>','<MaTuKhoaPhuong>5</MaTuKhoaPhuong>',1),('2013-11-10 12:37:48','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>15</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>15</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:37:49','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>16</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>16</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:37:49','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>17</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>17</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:37:47','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>18</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>18</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:37:47','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>19</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>19</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:37:48','TUKHOAQUANHUYEN','<MaTuKhoaQuanHuyen>20</MaTuKhoaQuanHuyen>','<MaTuKhoaQuanHuyen>20</MaTuKhoaQuanHuyen>',1),('2013-11-10 12:39:38','TUKHOADUONG','<MaTuKhoaDuong>11</MaTuKhoaDuong>','<MaTuKhoaDuong>11</MaTuKhoaDuong>',1),('2013-11-10 12:39:43','TUKHOADUONG','<MaTuKhoaDuong>12</MaTuKhoaDuong>','<MaTuKhoaDuong>12</MaTuKhoaDuong>',1),('2013-11-10 12:39:58','TUKHOADUONG','<MaTuKhoaDuong>13</MaTuKhoaDuong>','<MaTuKhoaDuong>13</MaTuKhoaDuong>',1),('2013-11-10 12:40:03','TENDIADIEM','<MaTenDiaDiem>76603</MaTenDiaDiem>','<MaTenDiaDiem>76603</MaTenDiaDiem>',1),('2013-11-10 12:40:40','DUONG','<MaDuong>1106</MaDuong>','<MaDuong>1106</MaDuong>',1),('2013-11-10 12:41:05','PHUONG','<MaPhuong>228</MaPhuong>','<MaPhuong>228</MaPhuong>',1),('2013-11-10 12:41:39','DULIEU','<MaDuLieu>15</MaDuLieu>','<MaDuLieu>15</MaDuLieu>',1),('2013-11-10 12:43:51','TENDIADIEM','<MaTenDiaDiem>76604</MaTenDiaDiem>','<MaTenDiaDiem>76604</MaTenDiaDiem>',1),('2013-11-10 12:44:52','DUONG','<MaDuong>1107</MaDuong>','<MaDuong>1107</MaDuong>',1),('2013-11-10 12:45:12','PHUONG','<MaPhuong>229</MaPhuong>','<MaPhuong>229</MaPhuong>',1),('2013-11-10 12:45:50','DULIEU','<MaDuLieu>16</MaDuLieu>','<MaDuLieu>16</MaDuLieu>',1),('2013-11-10 12:47:03','DUONG','<MaDuong>1109</MaDuong>','<MaDuong>1109</MaDuong>',1),('2013-11-10 12:47:40','TENDIADIEM','<MaTenDiaDiem>76605</MaTenDiaDiem>','<MaTenDiaDiem>76605</MaTenDiaDiem>',1),('2013-11-10 12:48:35','DULIEU','<MaDuLieu>17</MaDuLieu>','<MaDuLieu>17</MaDuLieu>',1),('2013-11-10 12:50:14','TENDIADIEM','<MaTenDiaDiem>76606</MaTenDiaDiem>','<MaTenDiaDiem>76606</MaTenDiaDiem>',1),('2013-11-10 12:51:25','DULIEU','<MaDuLieu>18</MaDuLieu>','<MaDuLieu>18</MaDuLieu>',1),('2013-11-10 12:53:37','TENDIADIEM','<MaTenDiaDiem>76607</MaTenDiaDiem>','<MaTenDiaDiem>76607</MaTenDiaDiem>',1),('2013-11-10 12:54:30','DULIEU','<MaDuLieu>19</MaDuLieu>','<MaDuLieu>19</MaDuLieu>',1),('2013-11-10 12:58:16','TENDIADIEM','<MaTenDiaDiem>76608</MaTenDiaDiem>','<MaTenDiaDiem>76608</MaTenDiaDiem>',1),('2013-11-10 12:58:48','DUONG','<MaDuong>1108</MaDuong>','<MaDuong>1108</MaDuong>',1),('2013-11-10 12:59:38','PHUONG','<MaPhuong>230</MaPhuong>','<MaPhuong>230</MaPhuong>',1),('2013-11-10 13:01:08','DULIEU','<MaDuLieu>20</MaDuLieu>','<MaDuLieu>20</MaDuLieu>',1),('2013-11-10 13:02:01','TENDIADIEM','<MaTenDiaDiem>76609</MaTenDiaDiem>','<MaTenDiaDiem>76609</MaTenDiaDiem>',1),('2013-11-10 13:02:54','PHUONG','<MaPhuong>231</MaPhuong>','<MaPhuong>231</MaPhuong>',1),('2013-11-10 13:03:46','DULIEU','<MaDuLieu>21</MaDuLieu>','<MaDuLieu>21</MaDuLieu>',1),('2013-11-10 13:06:17','DUONG','<MaDuong>1110</MaDuong>','<MaDuong>1110</MaDuong>',1),('2013-11-10 13:06:18','DUONG','<MaDuong>1111</MaDuong>','<MaDuong>1111</MaDuong>',1),('2013-11-10 13:06:18','DUONG','<MaDuong>1112</MaDuong>','<MaDuong>1112</MaDuong>',1),('2013-11-10 13:06:19','DUONG','<MaDuong>1113</MaDuong>','<MaDuong>1113</MaDuong>',1),('2013-11-10 13:06:20','DUONG','<MaDuong>1114</MaDuong>','<MaDuong>1114</MaDuong>',1),('2013-11-10 13:06:22','TENDIADIEM','<MaTenDiaDiem>76610</MaTenDiaDiem>','<MaTenDiaDiem>76610</MaTenDiaDiem>',1),('2013-11-10 13:08:10','DUONG','<MaDuong>1116</MaDuong>','<MaDuong>1116</MaDuong>',1),('2013-11-10 13:09:33','DULIEU','<MaDuLieu>22</MaDuLieu>','<MaDuLieu>22</MaDuLieu>',1),('2013-11-10 13:12:49','TENDIADIEM','<MaTenDiaDiem>76611</MaTenDiaDiem>','<MaTenDiaDiem>76611</MaTenDiaDiem>',1),('2013-11-10 13:13:41','DULIEU','<MaDuLieu>23</MaDuLieu>','<MaDuLieu>23</MaDuLieu>',1),('2013-11-10 13:14:31','TENDIADIEM','<MaTenDiaDiem>76612</MaTenDiaDiem>','<MaTenDiaDiem>76612</MaTenDiaDiem>',1),('2013-11-10 13:15:17','DULIEU','<MaDuLieu>24</MaDuLieu>','<MaDuLieu>24</MaDuLieu>',1),('2013-11-10 13:16:32','TENDIADIEM','<MaTenDiaDiem>76613</MaTenDiaDiem>','<MaTenDiaDiem>76613</MaTenDiaDiem>',1),('2013-11-10 13:17:22','DUONG','<MaDuong>1115</MaDuong>','<MaDuong>1115</MaDuong>',1),('2013-11-10 13:18:13','DULIEU','<MaDuLieu>25</MaDuLieu>','<MaDuLieu>25</MaDuLieu>',1),('2013-11-10 13:21:42','TENDIADIEM','<MaTenDiaDiem>76614</MaTenDiaDiem>','<MaTenDiaDiem>76614</MaTenDiaDiem>',1),('2013-11-10 13:22:06','DULIEU','<MaDuLieu>26</MaDuLieu>','<MaDuLieu>26</MaDuLieu>',1),('2013-11-10 13:24:41','DUONG','<MaDuong>1117</MaDuong>','<MaDuong>1117</MaDuong>',1),('2013-11-10 13:26:56','TENDIADIEM','<MaTenDiaDiem>76615</MaTenDiaDiem>','<MaTenDiaDiem>76615</MaTenDiaDiem>',1),('2013-11-10 13:28:00','DULIEU','<MaDuLieu>27</MaDuLieu>','<MaDuLieu>27</MaDuLieu>',1),('2013-11-10 13:29:44','TENDIADIEM','<MaTenDiaDiem>76616</MaTenDiaDiem>','<MaTenDiaDiem>76616</MaTenDiaDiem>',1),('2013-11-10 13:30:50','DULIEU','<MaDuLieu>28</MaDuLieu>','<MaDuLieu>28</MaDuLieu>',1),('2013-11-10 13:32:37','TENDIADIEM','<MaTenDiaDiem>76617</MaTenDiaDiem>','<MaTenDiaDiem>76617</MaTenDiaDiem>',1),('2013-11-10 13:33:29','DULIEU','<MaDuLieu>29</MaDuLieu>','<MaDuLieu>29</MaDuLieu>',1),('2013-11-10 13:43:52','DUONG','<MaDuong>1118</MaDuong>','<MaDuong>1118</MaDuong>',1),('2013-11-10 13:41:06','TENDIADIEM','<MaTenDiaDiem>76618</MaTenDiaDiem>','<MaTenDiaDiem>76618</MaTenDiaDiem>',1),('2013-11-10 13:41:40','DULIEU','<MaDuLieu>30</MaDuLieu>','<MaDuLieu>30</MaDuLieu>',1),('2013-11-10 13:43:53','DUONG','<MaDuong>1119</MaDuong>','<MaDuong>1119</MaDuong>',1),('2013-11-10 13:44:21','TENDIADIEM','<MaTenDiaDiem>76619</MaTenDiaDiem>','<MaTenDiaDiem>76619</MaTenDiaDiem>',1),('2013-11-10 13:45:29','DULIEU','<MaDuLieu>31</MaDuLieu>','<MaDuLieu>31</MaDuLieu>',1),('2013-11-10 15:05:27','TAIKHOAN','<MaTaiKhoan>19</MaTaiKhoan>','<MaTaiKhoan>19</MaTaiKhoan>',1),('2013-11-10 15:44:04','PHUONG','<MaPhuong>232</MaPhuong>','<MaPhuong>232</MaPhuong>',1),('2013-11-10 15:44:37','TUKHOAPHUONG','<MaTuKhoaPhuong>6</MaTuKhoaPhuong>','<MaTuKhoaPhuong>6</MaTuKhoaPhuong>',1),('2013-11-10 15:44:51','TUKHOAPHUONG','<MaTuKhoaPhuong>7</MaTuKhoaPhuong>','<MaTuKhoaPhuong>7</MaTuKhoaPhuong>',1),('2013-11-10 15:45:42','TUKHOAPHUONG','<MaTuKhoaPhuong>8</MaTuKhoaPhuong>','<MaTuKhoaPhuong>8</MaTuKhoaPhuong>',1),('2013-11-10 15:49:16','TENDIADIEM','<MaTenDiaDiem>76620</MaTenDiaDiem>','<MaTenDiaDiem>76620</MaTenDiaDiem>',1),('2013-11-10 15:57:55','TENDIADIEM','<MaTenDiaDiem>76621</MaTenDiaDiem>','<MaTenDiaDiem>76621</MaTenDiaDiem>',1),('2013-11-10 17:41:45','TENDIADIEM','<MaTenDiaDiem>76623</MaTenDiaDiem>','<MaTenDiaDiem>76623</MaTenDiaDiem>',1),('2013-11-10 17:43:26','DULIEU','<MaDuLieu>35</MaDuLieu>','<MaDuLieu>35</MaDuLieu>',1),('2013-11-10 17:46:43','TENDIADIEM','<MaTenDiaDiem>76624</MaTenDiaDiem>','<MaTenDiaDiem>76624</MaTenDiaDiem>',1),('2013-11-10 17:47:51','PHUONG','<MaPhuong>233</MaPhuong>','<MaPhuong>233</MaPhuong>',1),('2013-11-10 17:48:58','DULIEU','<MaDuLieu>36</MaDuLieu>','<MaDuLieu>36</MaDuLieu>',1),('2013-11-11 15:10:55','BINHLUAN','<MaBinhLuan>3</MaBinhLuan>','<MaBinhLuan>3</MaBinhLuan>',1),('2013-11-11 15:52:57','TAIKHOAN','<MaTaiKhoan>20</MaTaiKhoan>','<MaTaiKhoan>20</MaTaiKhoan>',1),('2013-11-11 17:15:58','BINHLUAN','<MaBinhLuan>4</MaBinhLuan>','<MaBinhLuan>4</MaBinhLuan>',1),('2013-11-11 17:35:42','BINHLUAN','<MaBinhLuan>5</MaBinhLuan>','<MaBinhLuan>5</MaBinhLuan>',1),('2013-11-11 17:37:42','BINHLUAN','<MaBinhLuan>6</MaBinhLuan>','<MaBinhLuan>6</MaBinhLuan>',1),('2013-11-11 17:52:29','BINHLUAN','<MaBinhLuan>7</MaBinhLuan>','<MaBinhLuan>7</MaBinhLuan>',1),('2013-11-11 17:52:38','BINHLUAN','<MaBinhLuan>8</MaBinhLuan>','<MaBinhLuan>8</MaBinhLuan>',1),('2013-11-11 18:19:30','BINHLUAN','<MaBinhLuan>9</MaBinhLuan>','<MaBinhLuan>9</MaBinhLuan>',1),('2013-11-11 18:19:35','BINHLUAN','<MaBinhLuan>10</MaBinhLuan>','<MaBinhLuan>10</MaBinhLuan>',1),('2013-11-11 18:19:37','BINHLUAN','<MaBinhLuan>11</MaBinhLuan>','<MaBinhLuan>11</MaBinhLuan>',1),('2013-11-11 19:17:17','BINHLUAN','<MaBinhLuan>12</MaBinhLuan>','<MaBinhLuan>12</MaBinhLuan>',1),('2013-11-11 19:17:18','BINHLUAN','<MaBinhLuan>13</MaBinhLuan>','<MaBinhLuan>13</MaBinhLuan>',1),('2013-11-11 19:17:42','BINHLUAN','<MaBinhLuan>14</MaBinhLuan>','<MaBinhLuan>14</MaBinhLuan>',1),('2013-11-12 09:02:51','BINHLUAN','<MaBinhLuan>15</MaBinhLuan>','<MaBinhLuan>15</MaBinhLuan>',1),('2013-11-12 15:37:59','BINHLUAN','<MaBinhLuan>16</MaBinhLuan>','<MaBinhLuan>16</MaBinhLuan>',1),('2013-11-12 15:49:57','BINHLUAN','<MaBinhLuan>17</MaBinhLuan>','<MaBinhLuan>17</MaBinhLuan>',1),('2013-11-12 15:57:05','BINHLUAN','<MaBinhLuan>18</MaBinhLuan>','<MaBinhLuan>18</MaBinhLuan>',1),('2013-11-12 16:04:53','BINHLUAN','<MaBinhLuan>19</MaBinhLuan>','<MaBinhLuan>19</MaBinhLuan>',1),('2013-11-12 16:11:35','BINHLUAN','<MaBinhLuan>20</MaBinhLuan>','<MaBinhLuan>20</MaBinhLuan>',1),('2013-11-12 16:14:05','BINHLUAN','<MaBinhLuan>21</MaBinhLuan>','<MaBinhLuan>21</MaBinhLuan>',1),('2013-11-12 18:28:09','BINHLUAN','<MaBinhLuan>22</MaBinhLuan>','<MaBinhLuan>22</MaBinhLuan>',1),('2013-11-12 18:50:04','TAIKHOAN','<MaTaiKhoan>21</MaTaiKhoan>','<MaTaiKhoan>21</MaTaiKhoan>',1),('2013-11-12 18:51:24','BINHLUAN','<MaBinhLuan>23</MaBinhLuan>','<MaBinhLuan>23</MaBinhLuan>',1),('2013-11-12 20:40:40','BINHLUAN','<MaBinhLuan>24</MaBinhLuan>','<MaBinhLuan>24</MaBinhLuan>',1),('2013-11-13 05:55:52','BINHLUAN','<MaBinhLuan>25</MaBinhLuan>','<MaBinhLuan>25</MaBinhLuan>',1),('2013-11-13 06:55:15','TAIKHOAN','<MaTaiKhoan>22</MaTaiKhoan>','<MaTaiKhoan>22</MaTaiKhoan>',1),('2013-11-13 06:57:58','BINHLUAN','<MaBinhLuan>26</MaBinhLuan>','<MaBinhLuan>26</MaBinhLuan>',1),('2013-11-16 16:51:50','DULIEU_YEUTHICH','<MaDuLieu>1</MaDuLieu><MaTaiKhoan>2</MaTaiKhoan>','<MaDuLieu>1</MaDuLieu><MaTaiKhoan>2</MaTaiKhoan>',1);
/*!40000 ALTER TABLE `history_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phuong`
--

DROP TABLE IF EXISTS `phuong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phuong` (
  `MaPhuong` int(11) NOT NULL AUTO_INCREMENT,
  `TenPhuong` varchar(64) DEFAULT '',
  PRIMARY KEY (`MaPhuong`),
  UNIQUE KEY `PHUONG_PHUONG$TenPhuong` (`TenPhuong`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phuong`
--

LOCK TABLES `phuong` WRITE;
/*!40000 ALTER TABLE `phuong` DISABLE KEYS */;
INSERT INTO `phuong` VALUES (227,'10'),(229,'11'),(230,'15'),(231,'17'),(226,'3'),(228,'4'),(220,'6'),(221,'7'),(222,'8'),(223,'9'),(225,'Bến Thành'),(210,'Nguyễn Cư Trinh'),(211,'Nguyễn Thái Bình'),(212,'Nhị Bình'),(213,'Nhơn Đức'),(214,'Phạm Ngũ Lão'),(215,'Phong Phú'),(216,'Phú Hữu'),(217,'Phú Mỹ'),(218,'Phú Thạnh'),(219,'Phú Thọ Hòa'),(224,'Tân Phong'),(233,'Tân Phú'),(232,'Tân Quy');
/*!40000 ALTER TABLE `phuong` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_PHUONG` AFTER INSERT ON `phuong`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'PHUONG'; 						SET @pk_d = CONCAT('<MaPhuong>',NEW.`MaPhuong`,'</MaPhuong>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_PHUONG` AFTER UPDATE ON `phuong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'PHUONG';						SET @pk_d_old = CONCAT('<MaPhuong>',OLD.`MaPhuong`,'</MaPhuong>');						SET @pk_d = CONCAT('<MaPhuong>',NEW.`MaPhuong`,'</MaPhuong>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_PHUONG` AFTER DELETE ON `phuong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'PHUONG';						SET @pk_d = CONCAT('<MaPhuong>',OLD.`MaPhuong`,'</MaPhuong>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `quanhuyen`
--

DROP TABLE IF EXISTS `quanhuyen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quanhuyen` (
  `MaQuanHuyen` int(11) NOT NULL AUTO_INCREMENT,
  `TenQuanHuyen` varchar(64) DEFAULT '',
  PRIMARY KEY (`MaQuanHuyen`),
  UNIQUE KEY `QUANHUYEN_QUANHUYEN$TenQuanHuyen` (`TenQuanHuyen`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quanhuyen`
--

LOCK TABLES `quanhuyen` WRITE;
/*!40000 ALTER TABLE `quanhuyen` DISABLE KEYS */;
INSERT INTO `quanhuyen` VALUES (26,'1'),(33,'10'),(28,'4'),(29,'5'),(30,'6'),(1,'7'),(31,'8'),(32,'9'),(44,'Bình Tân'),(46,'Bình Thạnh'),(45,'Gò Vấp'),(49,'Huyện Nhà Bè'),(48,'Phú Nhuận'),(27,'Tân Bình'),(34,'Tân Phú'),(47,'Thủ Đức');
/*!40000 ALTER TABLE `quanhuyen` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_QUANHUYEN` AFTER INSERT ON `quanhuyen`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'QUANHUYEN'; 						SET @pk_d = CONCAT('<MaQuanHuyen>',NEW.`MaQuanHuyen`,'</MaQuanHuyen>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_QUANHUYEN` AFTER UPDATE ON `quanhuyen`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'QUANHUYEN';						SET @pk_d_old = CONCAT('<MaQuanHuyen>',OLD.`MaQuanHuyen`,'</MaQuanHuyen>');						SET @pk_d = CONCAT('<MaQuanHuyen>',NEW.`MaQuanHuyen`,'</MaQuanHuyen>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_QUANHUYEN` AFTER DELETE ON `quanhuyen`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'QUANHUYEN';						SET @pk_d = CONCAT('<MaQuanHuyen>',OLD.`MaQuanHuyen`,'</MaQuanHuyen>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `taikhoan`
--

DROP TABLE IF EXISTS `taikhoan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `taikhoan` (
  `MaTaiKhoan` int(11) NOT NULL AUTO_INCREMENT,
  `TenTaiKhoan` varchar(64) DEFAULT '',
  `MatKhau` varchar(64) DEFAULT '',
  `HoVaTen` varchar(64) DEFAULT '',
  `EMAIL` varchar(64) DEFAULT '',
  `NGAYSINH` datetime DEFAULT NULL,
  PRIMARY KEY (`MaTaiKhoan`),
  UNIQUE KEY `UQ__TAIKHOAN__B106EAF87A8A2FE4` (`TenTaiKhoan`),
  KEY `TAIKHOAN_TAIKHOAN$TenTaiKhoan` (`TenTaiKhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `taikhoan`
--

LOCK TABLES `taikhoan` WRITE;
/*!40000 ALTER TABLE `taikhoan` DISABLE KEYS */;
INSERT INTO `taikhoan` VALUES (1,'thlong','123456','Tôn Hoàng Long','thlong@hotmail.com',NULL),(2,'pmnhut','1234567','','pmnhut@hotmail.com',NULL),(3,'vmnhut','123456','','vmnhut@hotmail.com',NULL),(4,'nmnhut','123456','','nmnhut@hotmail.com',NULL),(5,'nhphat','123456','Nguyễn Hòa Fuck','nhphat@hotmail.com','1111-11-11 00:00:00'),(6,'sdfd','sdfsfsdfd','sdfdsf','sdfdsfs','0000-00-00 00:00:00'),(7,'dfvfd','sdfsfsdfd','sdfdsf','sdfdsfs','0000-00-00 00:00:00'),(8,'dfvdfgfd','sdfsfsdfd','sdfdsf','sdfdsfs','0000-00-00 00:00:00'),(10,'dfvdfgfdfdfd','sdfsfsdfd','sdfdsf','sdfdsfs','1992-11-20 00:00:00'),(11,'nhphsdfgdfat','123456','Nguyễn Hòa Fuck','nhdfdsfsphat@hotmail.com','2011-11-11 00:00:00'),(12,'nhphsdfgddddfat','123456','Nguyễn Hòa Fuck','nhdfdsfsphat@hotmail.com','1992-11-16 00:00:00'),(13,'phanmn','te tò te','Phan Minh Nhựt','phanminhnhut@wego.com','1992-11-16 00:00:00'),(14,'hoaphat92','123456','Nguyen Hoa Phat','phatnguyen.it.khtn@gmail.com','1992-11-06 00:00:00'),(15,'nhut','nhut','nhut','nhut@yahoo.com','1980-01-01 00:00:00'),(16,'hoaphat','123456','phat','hoaphat92@gmail.com','1980-01-01 00:00:00'),(17,'hoaph','123456','phat','hoam@tail.com','1980-01-01 00:00:00'),(18,'dangson','password','Sơn','dangsongg@yahoo.com',NULL),(19,'123','qwe','nhut','nhut1@yahoo.com','1980-01-01 00:00:00'),(20,'nhom1','123456','nhom1','nhom1@yahoo.com','1992-02-01 00:00:00'),(21,'nhutvm','123456','Nhut','nhut99@yahoo.com','1992-01-29 00:00:00'),(22,'demo','123456','abcd','hoaphat@gmail.com','1980-01-01 00:00:00');
/*!40000 ALTER TABLE `taikhoan` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TAIKHOAN` AFTER INSERT ON `taikhoan`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TAIKHOAN'; 						SET @pk_d = CONCAT('<MaTaiKhoan>',NEW.`MaTaiKhoan`,'</MaTaiKhoan>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TAIKHOAN` AFTER UPDATE ON `taikhoan`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TAIKHOAN';						SET @pk_d_old = CONCAT('<MaTaiKhoan>',OLD.`MaTaiKhoan`,'</MaTaiKhoan>');						SET @pk_d = CONCAT('<MaTaiKhoan>',NEW.`MaTaiKhoan`,'</MaTaiKhoan>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TAIKHOAN` AFTER DELETE ON `taikhoan`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TAIKHOAN';						SET @pk_d = CONCAT('<MaTaiKhoan>',OLD.`MaTaiKhoan`,'</MaTaiKhoan>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tendiadiem`
--

DROP TABLE IF EXISTS `tendiadiem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tendiadiem` (
  `MaTenDiaDiem` int(11) NOT NULL AUTO_INCREMENT,
  `TenDiaDiem` varchar(128) DEFAULT '',
  PRIMARY KEY (`MaTenDiaDiem`)
) ENGINE=InnoDB AUTO_INCREMENT=76625 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tendiadiem`
--

LOCK TABLES `tendiadiem` WRITE;
/*!40000 ALTER TABLE `tendiadiem` DISABLE KEYS */;
INSERT INTO `tendiadiem` VALUES (76576,'039 LÔ B'),(76577,'047 LÔ B'),(76578,'100'),(76579,'101'),(76580,'102'),(76581,'104'),(76582,'107'),(76583,'108'),(76584,'110E/3 Nhà trọ'),(76585,'111BIS'),(76586,'114 Nhà Thiếu Nhi Quận Bình Thạnh'),(76587,'116'),(76588,'Sân bóng đá mini Hải Sơn'),(76589,'Trường THCS Gò Vấp'),(76590,'Karaoke Nice Phan Văn Trị'),(76591,'Công viên Gia Định'),(76592,'Trường THPT Nguyễn Công Trứ'),(76594,'lotteria'),(76595,'texas chicken'),(76596,'công viên hồ bán nguyệt'),(76597,'Công viên 23 tháng 9'),(76598,'Khách Sạn New World Sài Gòn'),(76599,'Khách Sạn Cinnamon Sài Gòn'),(76600,'Trường THPT Gò Vấp'),(76601,'Nhà Hàng Hương Phố'),(76602,'Siêu thị Văn Lang'),(76603,'Trường tiểu học Hạnh Thông'),(76604,'Trường THCS Nguyễn Du'),(76605,'Trường Đại học Công nghiệp'),(76606,'Chợ Gò Vấp'),(76607,'Công an Quận Gò Vấp'),(76608,'Bệnh viện Gò Vấp'),(76609,'Karaoke Táo Đỏ'),(76610,'ATM Sacombank Nguyễn Oanh'),(76611,'Trường THCS Quang Trung'),(76612,'Cafe Country Houses'),(76613,'Siêu thị BigC Nguyễn Kiệm'),(76614,'Trường tiểu học Trần Văn Ơn - Gò Vấp'),(76615,'Café Cát Đằng - Gò Vấp'),(76616,'Karaoke Nhân Sao'),(76617,'Trường THPT Nguyễn Trung Trực'),(76618,'Trường THPT Lê Hồng Phong'),(76619,'Công viên Tao Đàn'),(76620,'Karaoke MiMi 6'),(76621,'Co.opMart Phú Mỹ Hưng'),(76622,'ATM Sacombank - PMH'),(76623,'Đại học Tôn Đức Thắng'),(76624,'Cresent Mall');
/*!40000 ALTER TABLE `tendiadiem` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TENDIADIEM` AFTER INSERT ON `tendiadiem`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TENDIADIEM'; 						SET @pk_d = CONCAT('<MaTenDiaDiem>',NEW.`MaTenDiaDiem`,'</MaTenDiaDiem>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TENDIADIEM` AFTER UPDATE ON `tendiadiem`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TENDIADIEM';						SET @pk_d_old = CONCAT('<MaTenDiaDiem>',OLD.`MaTenDiaDiem`,'</MaTenDiaDiem>');						SET @pk_d = CONCAT('<MaTenDiaDiem>',NEW.`MaTenDiaDiem`,'</MaTenDiaDiem>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TENDIADIEM` AFTER DELETE ON `tendiadiem`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TENDIADIEM';						SET @pk_d = CONCAT('<MaTenDiaDiem>',OLD.`MaTenDiaDiem`,'</MaTenDiaDiem>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tinhthanh`
--

DROP TABLE IF EXISTS `tinhthanh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tinhthanh` (
  `MaTinhThanh` int(11) NOT NULL AUTO_INCREMENT,
  `TenTinhThanh` varchar(64) DEFAULT '',
  PRIMARY KEY (`MaTinhThanh`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tinhthanh`
--

LOCK TABLES `tinhthanh` WRITE;
/*!40000 ALTER TABLE `tinhthanh` DISABLE KEYS */;
INSERT INTO `tinhthanh` VALUES (16,'Hồ Chí Minh');
/*!40000 ALTER TABLE `tinhthanh` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TINHTHANH` AFTER INSERT ON `tinhthanh`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TINHTHANH'; 						SET @pk_d = CONCAT('<MaTinhThanh>',NEW.`MaTinhThanh`,'</MaTinhThanh>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TINHTHANH` AFTER UPDATE ON `tinhthanh`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TINHTHANH';						SET @pk_d_old = CONCAT('<MaTinhThanh>',OLD.`MaTinhThanh`,'</MaTinhThanh>');						SET @pk_d = CONCAT('<MaTinhThanh>',NEW.`MaTinhThanh`,'</MaTinhThanh>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TINHTHANH` AFTER DELETE ON `tinhthanh`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TINHTHANH';						SET @pk_d = CONCAT('<MaTinhThanh>',OLD.`MaTinhThanh`,'</MaTinhThanh>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tukhoadiadiem`
--

DROP TABLE IF EXISTS `tukhoadiadiem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tukhoadiadiem` (
  `MaTuKhoaTenDiaDiem` int(11) NOT NULL AUTO_INCREMENT,
  `TuKhoaTenDiaDiem` varchar(128) DEFAULT '',
  `MaTenDiaDiem` int(11) DEFAULT NULL,
  PRIMARY KEY (`MaTuKhoaTenDiaDiem`),
  KEY `TUKHOADIADIEM_TUKHOADIADIEM$MaTenDiaDiem1` (`MaTenDiaDiem`),
  KEY `TUKHOADIADIEM_TUKHOADIADIEM$TuKhoaTenDiaDiem` (`TuKhoaTenDiaDiem`),
  CONSTRAINT `TUKHOADIADIEM$MaTenDiaDiem` FOREIGN KEY (`MaTenDiaDiem`) REFERENCES `tendiadiem` (`MaTenDiaDiem`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tukhoadiadiem`
--

LOCK TABLES `tukhoadiadiem` WRITE;
/*!40000 ALTER TABLE `tukhoadiadiem` DISABLE KEYS */;
INSERT INTO `tukhoadiadiem` VALUES (1,'huong pho',76601),(2,'hai son',76588),(3,'nice phan van tri',76590),(4,'Nice Phan Văn Trị',76590),(5,'Hương Phố',76601),(6,'Nhà hàng Hương Phố',76601),(7,'Nha hang huong pho',76601);
/*!40000 ALTER TABLE `tukhoadiadiem` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TUKHOADIADIEM` AFTER INSERT ON `tukhoadiadiem`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADIADIEM'; 						SET @pk_d = CONCAT('<MaTuKhoaTenDiaDiem>',NEW.`MaTuKhoaTenDiaDiem`,'</MaTuKhoaTenDiaDiem>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TUKHOADIADIEM` AFTER UPDATE ON `tukhoadiadiem`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADIADIEM';						SET @pk_d_old = CONCAT('<MaTuKhoaTenDiaDiem>',OLD.`MaTuKhoaTenDiaDiem`,'</MaTuKhoaTenDiaDiem>');						SET @pk_d = CONCAT('<MaTuKhoaTenDiaDiem>',NEW.`MaTuKhoaTenDiaDiem`,'</MaTuKhoaTenDiaDiem>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TUKHOADIADIEM` AFTER DELETE ON `tukhoadiadiem`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADIADIEM';						SET @pk_d = CONCAT('<MaTuKhoaTenDiaDiem>',OLD.`MaTuKhoaTenDiaDiem`,'</MaTuKhoaTenDiaDiem>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tukhoadichvu`
--

DROP TABLE IF EXISTS `tukhoadichvu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tukhoadichvu` (
  `MaTuKhoaDichVu` int(11) NOT NULL AUTO_INCREMENT,
  `TuKhoaDichVu` varchar(64) DEFAULT '',
  `MaDichVu` int(11) DEFAULT NULL,
  PRIMARY KEY (`MaTuKhoaDichVu`),
  KEY `TUKHOADICHVU_TUKHOADICHVU$MaDichVu1` (`MaDichVu`),
  KEY `TUKHOADICHVU_TUKHOADICHVU$MaDichVu2` (`MaDichVu`),
  KEY `TUKHOADICHVU_TUKHOADICHVU$TuKhoaDichVu` (`TuKhoaDichVu`),
  CONSTRAINT `TUKHOADICHVU$MaDichVu` FOREIGN KEY (`MaDichVu`) REFERENCES `dichvu` (`MaDichVu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tukhoadichvu`
--

LOCK TABLES `tukhoadichvu` WRITE;
/*!40000 ALTER TABLE `tukhoadichvu` DISABLE KEYS */;
INSERT INTO `tukhoadichvu` VALUES (1,'Sân banh',15),(2,'Sân mini',15),(3,'Sân đá banh',15),(4,'Trường',2),(5,'Trường học',2),(6,'Karaoke',9),(7,'Công viên',12),(8,'Quán ăn',4),(10,'Nhà hàng',10),(11,'nha hang',10),(12,'San mini',15),(13,'san banh',15),(15,'Truong',2),(16,'cong vien',12),(18,'Quan an',4);
/*!40000 ALTER TABLE `tukhoadichvu` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TUKHOADICHVU` AFTER INSERT ON `tukhoadichvu`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADICHVU'; 						SET @pk_d = CONCAT('<MaTuKhoaDichVu>',NEW.`MaTuKhoaDichVu`,'</MaTuKhoaDichVu>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TUKHOADICHVU` AFTER UPDATE ON `tukhoadichvu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADICHVU';						SET @pk_d_old = CONCAT('<MaTuKhoaDichVu>',OLD.`MaTuKhoaDichVu`,'</MaTuKhoaDichVu>');						SET @pk_d = CONCAT('<MaTuKhoaDichVu>',NEW.`MaTuKhoaDichVu`,'</MaTuKhoaDichVu>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TUKHOADICHVU` AFTER DELETE ON `tukhoadichvu`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADICHVU';						SET @pk_d = CONCAT('<MaTuKhoaDichVu>',OLD.`MaTuKhoaDichVu`,'</MaTuKhoaDichVu>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tukhoaduong`
--

DROP TABLE IF EXISTS `tukhoaduong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tukhoaduong` (
  `MaTuKhoaDuong` int(11) NOT NULL AUTO_INCREMENT,
  `TuKhoaDuong` varchar(64) DEFAULT '',
  `MaDuong` int(11) DEFAULT NULL,
  PRIMARY KEY (`MaTuKhoaDuong`),
  KEY `TUKHOADUONG_TUKHOADUONG$MaDuong1` (`MaDuong`),
  KEY `TUKHOADUONG_TUKHOADUONG$TuKhoaDuong` (`TuKhoaDuong`),
  CONSTRAINT `TUKHOADUONG$MaDuong` FOREIGN KEY (`MaDuong`) REFERENCES `duong` (`MaDuong`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tukhoaduong`
--

LOCK TABLES `tukhoaduong` WRITE;
/*!40000 ALTER TABLE `tukhoaduong` DISABLE KEYS */;
INSERT INTO `tukhoaduong` VALUES (1,'Nguyen Du',1096),(2,'Nguyễn Du',1096),(3,'nguyen du',1096),(4,'Đường 20',1095),(5,'20',1095),(6,'Giám',1098),(7,'Hoàng Minh Giám',1098),(8,'Đường 20 - Quang Trung',1099),(9,'ton dat tien',1105),(10,'Tôn Dật Tiên',1105),(11,'nguyen duc canh',1101),(12,'Nguyễn Đức Cảnh',1101),(13,'Nguyen Duc Canh',1101);
/*!40000 ALTER TABLE `tukhoaduong` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TUKHOADUONG` AFTER INSERT ON `tukhoaduong`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADUONG'; 						SET @pk_d = CONCAT('<MaTuKhoaDuong>',NEW.`MaTuKhoaDuong`,'</MaTuKhoaDuong>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TUKHOADUONG` AFTER UPDATE ON `tukhoaduong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADUONG';						SET @pk_d_old = CONCAT('<MaTuKhoaDuong>',OLD.`MaTuKhoaDuong`,'</MaTuKhoaDuong>');						SET @pk_d = CONCAT('<MaTuKhoaDuong>',NEW.`MaTuKhoaDuong`,'</MaTuKhoaDuong>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TUKHOADUONG` AFTER DELETE ON `tukhoaduong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOADUONG';						SET @pk_d = CONCAT('<MaTuKhoaDuong>',OLD.`MaTuKhoaDuong`,'</MaTuKhoaDuong>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tukhoaphuong`
--

DROP TABLE IF EXISTS `tukhoaphuong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tukhoaphuong` (
  `MaTuKhoaPhuong` int(11) NOT NULL AUTO_INCREMENT,
  `TuKhoaPhuong` varchar(64) DEFAULT '',
  `MaPhuong` int(11) DEFAULT NULL,
  PRIMARY KEY (`MaTuKhoaPhuong`),
  KEY `TUKHOAPHUONG_TUKHOAPHUONG$MaPhuong1` (`MaPhuong`),
  KEY `TUKHOAPHUONG_TUKHOAPHUONG$TuKhoaPhuong` (`TuKhoaPhuong`),
  KEY `TuKhoaPhuong` (`TuKhoaPhuong`),
  KEY `TuKhoaPhuong_2` (`TuKhoaPhuong`),
  CONSTRAINT `TUKHOAPHUONG$MaPhuong` FOREIGN KEY (`MaPhuong`) REFERENCES `phuong` (`MaPhuong`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tukhoaphuong`
--

LOCK TABLES `tukhoaphuong` WRITE;
/*!40000 ALTER TABLE `tukhoaphuong` DISABLE KEYS */;
INSERT INTO `tukhoaphuong` VALUES (1,'7',221),(2,'8',222),(3,'9',223),(4,'Tân Phong',224),(5,'tan phong',224),(6,'tan quy',232),(7,'Tân Quy',232),(8,'Tan Quy',232);
/*!40000 ALTER TABLE `tukhoaphuong` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TUKHOAPHUONG` AFTER INSERT ON `tukhoaphuong`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOAPHUONG'; 						SET @pk_d = CONCAT('<MaTuKhoaPhuong>',NEW.`MaTuKhoaPhuong`,'</MaTuKhoaPhuong>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TUKHOAPHUONG` AFTER UPDATE ON `tukhoaphuong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOAPHUONG';						SET @pk_d_old = CONCAT('<MaTuKhoaPhuong>',OLD.`MaTuKhoaPhuong`,'</MaTuKhoaPhuong>');						SET @pk_d = CONCAT('<MaTuKhoaPhuong>',NEW.`MaTuKhoaPhuong`,'</MaTuKhoaPhuong>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TUKHOAPHUONG` AFTER DELETE ON `tukhoaphuong`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOAPHUONG';						SET @pk_d = CONCAT('<MaTuKhoaPhuong>',OLD.`MaTuKhoaPhuong`,'</MaTuKhoaPhuong>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tukhoaquanhuyen`
--

DROP TABLE IF EXISTS `tukhoaquanhuyen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tukhoaquanhuyen` (
  `MaTuKhoaQuanHuyen` int(11) NOT NULL AUTO_INCREMENT,
  `TuKhoaQuanHuyen` varchar(64) DEFAULT '',
  `MaQuanHuyen` int(11) DEFAULT NULL,
  PRIMARY KEY (`MaTuKhoaQuanHuyen`),
  KEY `TUKHOAQUANHUYEN_TUKHOAQUANHUYEN$MaQuanHuyen1` (`MaQuanHuyen`),
  KEY `TUKHOAQUANHUYEN_TUKHOAQUANHUYEN$TuKhoaQuanHuyen` (`TuKhoaQuanHuyen`),
  CONSTRAINT `TUKHOAQUANHUYEN$MaQuanHuyen` FOREIGN KEY (`MaQuanHuyen`) REFERENCES `quanhuyen` (`MaQuanHuyen`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tukhoaquanhuyen`
--

LOCK TABLES `tukhoaquanhuyen` WRITE;
/*!40000 ALTER TABLE `tukhoaquanhuyen` DISABLE KEYS */;
INSERT INTO `tukhoaquanhuyen` VALUES (1,'Q.GV',45),(2,'Gò Vấp',45),(3,'Bình Thạnh',46),(4,'Q.BT',46),(5,'PN',48),(6,'Phú Nhuận',48),(7,'Q.PN',48),(8,'Go Vap',45),(9,'go vap',45),(10,'phu nhuan',48),(11,'Phu Nhuan',48),(12,'quan 7',1),(13,'quận 7',1),(14,'Quận 7',1),(15,'thanh',46),(16,'phu',48),(17,'nhuan',48),(18,'go',45),(19,'vap',45),(20,'binh',46);
/*!40000 ALTER TABLE `tukhoaquanhuyen` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TUKHOAQUANHUYEN` AFTER INSERT ON `tukhoaquanhuyen`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOAQUANHUYEN'; 						SET @pk_d = CONCAT('<MaTuKhoaQuanHuyen>',NEW.`MaTuKhoaQuanHuyen`,'</MaTuKhoaQuanHuyen>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TUKHOAQUANHUYEN` AFTER UPDATE ON `tukhoaquanhuyen`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOAQUANHUYEN';						SET @pk_d_old = CONCAT('<MaTuKhoaQuanHuyen>',OLD.`MaTuKhoaQuanHuyen`,'</MaTuKhoaQuanHuyen>');						SET @pk_d = CONCAT('<MaTuKhoaQuanHuyen>',NEW.`MaTuKhoaQuanHuyen`,'</MaTuKhoaQuanHuyen>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TUKHOAQUANHUYEN` AFTER DELETE ON `tukhoaquanhuyen`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOAQUANHUYEN';						SET @pk_d = CONCAT('<MaTuKhoaQuanHuyen>',OLD.`MaTuKhoaQuanHuyen`,'</MaTuKhoaQuanHuyen>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `tukhoatinhthanh`
--

DROP TABLE IF EXISTS `tukhoatinhthanh`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tukhoatinhthanh` (
  `MaTuKhoaTinhThanh` int(11) NOT NULL AUTO_INCREMENT,
  `TuKhoaTinhThanh` varchar(64) DEFAULT '',
  `MaTinhThanh` int(11) DEFAULT NULL,
  PRIMARY KEY (`MaTuKhoaTinhThanh`),
  KEY `TUKHOATINHTHANH_TUKHOATINHTHANH$MaTinhThanh1` (`MaTinhThanh`),
  KEY `TUKHOATINHTHANH_TUKHOATINHTHANH$TuKhoaTinhThanh` (`TuKhoaTinhThanh`),
  CONSTRAINT `TUKHOATINHTHANH$MaTinhThanh` FOREIGN KEY (`MaTinhThanh`) REFERENCES `tinhthanh` (`MaTinhThanh`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tukhoatinhthanh`
--

LOCK TABLES `tukhoatinhthanh` WRITE;
/*!40000 ALTER TABLE `tukhoatinhthanh` DISABLE KEYS */;
/*!40000 ALTER TABLE `tukhoatinhthanh` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_i_TUKHOATINHTHANH` AFTER INSERT ON `tukhoatinhthanh`
 FOR EACH ROW BEGIN 						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOATINHTHANH'; 						SET @pk_d = CONCAT('<MaTuKhoaTinhThanh>',NEW.`MaTuKhoaTinhThanh`,'</MaTuKhoaTinhThanh>'); 						SET @rec_state = 1;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`,`record_state` ) 						VALUES (@time_mark, @tbl_name, @pk_d, @pk_d, @rec_state); 						END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_u_TUKHOATINHTHANH` AFTER UPDATE ON `tukhoatinhthanh`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOATINHTHANH';						SET @pk_d_old = CONCAT('<MaTuKhoaTinhThanh>',OLD.`MaTuKhoaTinhThanh`,'</MaTuKhoaTinhThanh>');						SET @pk_d = CONCAT('<MaTuKhoaTinhThanh>',NEW.`MaTuKhoaTinhThanh`,'</MaTuKhoaTinhThanh>');						SET @rec_state = 2;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						IF @rs = 0 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d_old, @rec_state );						ELSE 						UPDATE `history_store` SET `timemark` = @time_mark, `pk_date_src` = @pk_d WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d_old;						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`viemde`@`%`*/ /*!50003 TRIGGER `a_d_TUKHOATINHTHANH` AFTER DELETE ON `tukhoatinhthanh`
 FOR EACH ROW BEGIN						SET @time_mark = DATE_ADD(NOW(), INTERVAL 0 SECOND); 						SET @tbl_name = 'TUKHOATINHTHANH';						SET @pk_d = CONCAT('<MaTuKhoaTinhThanh>',OLD.`MaTuKhoaTinhThanh`,'</MaTuKhoaTinhThanh>');						SET @rec_state = 3;						SET @rs = 0;						SELECT `record_state` INTO @rs FROM `history_store` WHERE  `table_name` = @tbl_name AND `pk_date_src` = @pk_d;						DELETE FROM `history_store` WHERE `table_name` = @tbl_name AND `pk_date_src` = @pk_d; 						IF @rs <> 1 THEN 						INSERT INTO `history_store`( `timemark`, `table_name`, `pk_date_src`,`pk_date_dest`, `record_state` ) VALUES (@time_mark, @tbl_name, @pk_d,@pk_d, @rec_state ); 						END IF; END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'wego_rest_db'
--
/*!50003 DROP PROCEDURE IF EXISTS `addLovedPlace` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `addLovedPlace`(IN userName nvarchar(64), IN MaDuLieu int)
BEGIN
	
	DECLARE accountId int;
	
	Select mataikhoan into accountId
	from taikhoan
	where TenTaiKhoan = userName;
	
	if accountId is not null
	then
		INSERT INTO `wego_rest_db`.`DuLieu_YeuThich`(`MaDuLieu`, `MaTaiKhoan`)
		VALUES( MaDuLieu, accountId);
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `addUserComment` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `addUserComment`(userName nvarchar(64), locationId int, commentContent nvarchar(500))
BEGIN
	DECLARE accountId int;
	
	Select mataikhoan into accountId
	from taikhoan
	where TenTaiKhoan = userName;
	
	if accountId is not null
	then
		INSERT INTO `wego_rest_db`.`binhluan`(`MaTaiKhoan`,`MaDuLieu`,`NoiDung`,`THOIGIAN`)
		VALUES( accountId, locationId, commentContent, now());
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `checkEmailExist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `checkEmailExist`(email nvarchar(64), out count int)
BEGIN
	select count(*) into count from taikhoan where taikhoan.email like email;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `checkUserNameExist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `checkUserNameExist`(userName nvarchar(64), out count  int)
BEGIN
	select count(*) into count from taikhoan where TenTaiKhoan like userName;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getFeeling` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `getFeeling`(IN userName nvarchar(64), IN maDuLieuVar int, Out feeling int)
BEGIN
	Select count(*) into feeling
	From camxuc_dulieu as cx
	Where cx.madulieu = maDuLieuVar 
	and cx.mataikhoan IN
						(   
							Select MaTaiKhoan
							from TaiKhoan as TK
							where TK.TenTaiKhoan=userName
						);
	
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getFeelingMTK` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `getFeelingMTK`(IN maTaiKhoanVar int, IN maDuLieuVar int, Out feeling int)
BEGIN
	Select count(*) into feeling
	From camxuc_dulieu as cx
	Where cx.madulieu = maDuLieuVar and cx.mataikhoan = maTaiKhoanVar;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getServiceDetails` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `getServiceDetails`(IN maDuLieuVar int)
BEGIN
	Select Ten, GiaTien, ChuThich
	From ChiTietDichVu as CTDV
	Where CTDV.MaChiTiet in 
	(
		Select MaChiTiet
		From DuLieu as DL, ChiTiet_DuLieu as CTDL
		Where DL.MaDuLieu = maDuLieuVar and CTDL.MaDuLieu = DL.MaDuLieu
	);
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getUserComments` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `getUserComments`(locationId int, currentPage int)
BEGIN
	declare indexStart int default 0;
	set indexStart = (currentPage - 1) * 20;
	select binhluan.*, taikhoan.tentaikhoan
	from binhluan, taikhoan
	where binhluan.mataikhoan = taikhoan.mataikhoan and binhluan.MaDuLieu = locationId
	order by binhluan.thoigian desc
	limit indexStart, 20;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `Login`(userName nvarchar(64), passWord nvarchar(64))
BEGIN
	SELECT * FROM taikhoan where TenTaiKhoan = userName and MatKhau = passWord;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Register` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `Register`(userName nvarchar(64), passWord nvarchar(64), fullName nvarchar(64), email nvarchar(64), dateOfBirth datetime)
BEGIN
	INSERT INTO TAIKHOAN (TenTaiKhoan, MatKhau, HoVaTen, Email, NgaySinh)
	VALUES (userName,passWord, fullName, email, dateOfBirth);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `searchDulieu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `searchDulieu`(IN matinhthanh_Var nvarchar(32), IN maquanhuyen_Var nvarchar(32), IN maphuong_Var nvarchar(32), IN maduong_Var nvarchar(32), IN matendiadiem_Var nvarchar(32), IN madichvu_Var nvarchar(32), IN fromIndex int)
BEGIN
	select MaDuLieu, SoNha, KinhDo, ViDo, ChuThich, SoLuotThich, TenTinhThanh, TenDichVu, TenDiaDiem, TenPhuong, TenDuong, TenQuanHuyen
	
	From DuLieu, DichVu, Phuong, QuanHuyen, TinhThanh, Duong, TenDiaDiem 
	
	Where
		DuLieu.MaDuLieu >= fromIndex
		AND
		DuLieu.MaDichVu = DichVu.MaDichVu
		AND
		FIND_IN_SET(CAST(DuLieu.MaDichVu as char(10)), if(CHAR_LENGTH(madichvu_Var) > 0, madichvu_Var, CAST(DuLieu.MaDichVu as char(10)))) >= 1
		And
		DuLieu.matendiadiem = TenDiaDiem.matendiadiem
		AND
		FIND_IN_SET(CAST(DuLieu.matendiadiem as char(10)), if(CHAR_LENGTH(matendiadiem_Var) > 0, matendiadiem_Var, CAST(DuLieu.matendiadiem as char(10)))) >= 1
		And
		DuLieu.maduong = Duong.maduong
		AND
		FIND_IN_SET(CAST(DuLieu.maduong as char(10)), if(CHAR_LENGTH(maduong_Var) > 0, maduong_Var, CAST(DuLieu.maduong as char(10)))) >= 1
		And
		DuLieu.maphuong = Phuong.maphuong
		AND
		FIND_IN_SET(CAST(DuLieu.maphuong as char(10)), if(CHAR_LENGTH(maphuong_Var) > 0, maphuong_Var, CAST(DuLieu.maphuong as char(10)))) >= 1
		AND
		DuLieu.maquanhuyen = QuanHuyen.maquanhuyen
		AND
		FIND_IN_SET(CAST(DuLieu.maquanhuyen as char(10)), if(CHAR_LENGTH(maquanhuyen_Var) > 0, maquanhuyen_Var, CAST(DuLieu.maquanhuyen as char(10)))) >= 1
		AND
		DuLieu.matinhthanh = TinhThanh.matinhthanh
		AND
		FIND_IN_SET(CAST(DuLieu.matinhthanh as char(10)), if(CHAR_LENGTH(matinhthanh_Var) > 0, matinhthanh_Var, CAST(DuLieu.matinhthanh as char(10)))) >= 1
	LIMIT 20;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `searchTableWithKey` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `searchTableWithKey`(tableName nvarchar(64), tukhoa nvarchar(256))
BEGIN
	case tableName
		when "tukhoaquanhuyen" then select maquanhuyen as id, tukhoaquanhuyen as tukhoadung from tukhoaquanhuyen where instr(tukhoa,tukhoaquanhuyen) > 0;
		when "tukhoatinhthanh" then select matinhthanh as id, tukhoatinhthanh as tukhoadung from tukhoatinhthanh where instr(tukhoa,tukhoatinhthanh) > 0;
		when "tukhoaphuong" then select maphuong as id, tukhoaphuong as tukhoadung from tukhoaphuong where instr(tukhoa,tukhoaphuong) > 0;
		when "tukhoaduong" then select maduong as id, tukhoaduong as tukhoadung from tukhoaduong where instr(tukhoa,tukhoaduong) > 0;
		when "tukhoadichvu" then select madichvu as id, tukhoadichvu as tukhoadung from tukhoadichvu where instr(tukhoa,tukhoadichvu) > 0;
		when "tukhoadiadiem" then select matendiadiem as id, tukhoatendiadiem as tukhoadung from tukhoadiadiem where instr(tukhoa,tukhoatendiadiem) > 0;
	end case;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `setFeeling` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `setFeeling`(IN userName nvarchar(64), IN maDuLieuVar int, IN feeling int)
BEGIN
	DECLARE accountId int;
	Declare rowCount int;
	Select mataikhoan into accountId
	from taikhoan
	where TenTaiKhoan = userName;
	
	if accountId is not null
	then
		if feeling = 1
		then
			start transaction;
			INSERT INTO `wego_rest_db`.`camxuc_dulieu`(`MaDuLieu`, `MaTaiKhoan`)
			VALUES( maDuLieuVar, accountId);
			Select ROW_COUNT() into rowCount;
			if(rowCount > 0) then
				update dulieu
				set SoLuotThich = SoLuotThich + 1
				where dulieu.madulieu = maDuLieuVar;
			end if;
			commit;
			select rowCount;
		else
			start transaction;
			delete 
			from camxuc_dulieu
			where MaTaiKhoan = accountId and MaDuLieu = maDuLieuVar;
			Select ROW_COUNT() into rowCount;
			if(rowCount > 0) then
				update dulieu
				set SoLuotThich = SoLuotThich - 1
				where dulieu.madulieu = maDuLieuVar;
			end if;
			commit;
			select rowCount;
		end if;
	end if;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `updatePassw` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `updatePassw`(IN userName nvarchar(64), IN oldPassw nvarchar(64), IN newPassw nvarchar(64))
BEGIN
	update taikhoan
	set MatKhau = newPassw
	where TenTaiKhoan = userName and MatKhau = oldPassw;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `viewLovedPlaces` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`viemde`@`%` PROCEDURE `viewLovedPlaces`(IN userName nvarchar(64), IN fromIndex int)
BEGIN
	select MaDuLieu, SoNha, KinhDo, ViDo, ChuThich, SoLuotThich, TenTinhThanh, TenDichVu, TenDiaDiem, TenPhuong, TenDuong, TenQuanHuyen
	
	From DuLieu, DichVu, Phuong, QuanHuyen, TinhThanh, Duong, TenDiaDiem
	
	Where
		DuLieu.MaDuLieu >= fromIndex
		AND
		DuLieu.MaDuLieu IN
						(   
							Select DLYT.MaDuLieu
							from TaiKhoan as TK, DuLieu_YeuThich as DLYT
							where TK.TenTaiKhoan=userName and TK.MaTaiKhoan = DLYT.MaTaiKhoan
						)
		AND
		DuLieu.MaDichVu = DichVu.MaDichVu
		And
		DuLieu.matendiadiem = TenDiaDiem.matendiadiem		
		And
		DuLieu.maduong = Duong.maduong
		And
		DuLieu.maphuong = Phuong.maphuong
		AND
		DuLieu.maquanhuyen = QuanHuyen.maquanhuyen
		AND
		DuLieu.matinhthanh = TinhThanh.matinhthanh
		
	LIMIT 20;
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-17 13:40:13
