/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.Place;

import CoreInterface.Place.IViewingServiceDetails;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("place/details")
public class ServiceDetails {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ViewingServiceDetails
     */
    public ServiceDetails() {
    }

    @GET
    @Produces("application/json; charset=utf-8")
    public String getJson(@QueryParam("token") String token, @QueryParam("ma_du_lieu") int maDuLieu) 
    {
        IViewingServiceDetails f = new Core.ViewingServiceDetails();
        
        return f.GetServiceDetails(token, maDuLieu);
    }
}
