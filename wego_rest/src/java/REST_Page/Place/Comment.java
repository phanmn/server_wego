/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.Place;

import BasicStruct.DetailsComment;
import CoreInterface.Place.IGettingComment;
import CoreInterface.Place.IPostingComment;
import JsonTemplate.JsonUtility;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("place/comment")
public class Comment {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Commenting
     */
    public Comment() {
    }

   
    @POST
    @Consumes("application/json")
    public String putJson(String content, @QueryParam("token") String token) 
    {
        Gson gson = new Gson();
        content = JsonUtility.StandardlizeJson(content);
        DetailsComment cm = null;
        IPostingComment f = new Core.PostingComment(token);
        
        content = JsonUtility.StandardlizeJson(content);
        try
        {
            cm = gson.fromJson(content, DetailsComment.class);
        }
        catch(JsonSyntaxException e)
        {
            System.out.println(e.toString());
        }
        
        return f.PostComment(cm);
    }
    
    @GET
    @Produces("application/json; charset=utf-8")
    public String getJson(@QueryParam("token") String token, @DefaultValue("0") @QueryParam("ma_du_lieu") int maDuLieu, @DefaultValue("-1") @QueryParam("index") int indexV) 
    {
       IGettingComment f = new Core.GettingComment(token);
        
       return f.GetComment(maDuLieu, indexV);
    }
}
