/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.Place;

import CoreAbstract.Place.AFindingPlace;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("place/find")
public class FindingPlace {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of FindingPlace
     */
    public FindingPlace() {
    }

   
    @GET
    @Produces("application/json; charset=utf-8")
    public String getJson(@QueryParam("token") String token, @QueryParam("query") String queryString, @QueryParam("index") int indexV) 
    {
        // expect "query"'s value condition: lower case, just have spaces, numbers, and words
        //return "{\"success\" :\"true\",\"content\" :[{\"ma_du_lieu\" :\"123\",\"ten_dich_vu\" :\"nhà hàng\",\"ten_dia_diem\" :\"Bitexco\",\"so_nha\" :\"B2D\",\"ten_duong\" :\"Đông Triều\",\"ten_phuong\" :\"1\",\"ten_quan_huyen\" :\"Quận 1\",\"ten_tinh_thanh\" :\"Hồ Chí Minh\",\"kinh_do\" :\"10\",\"vi_do\" :\"20\",\"chu_thich\" :\"Mắt tiền\",\"so_luot_thich\" :\"30\"},{\"ma_du_lieu\" :\"124\",\"ten_dich_vu\" :\"khách sạn\",\"ten_dia_diem\" :\"Bitexco\",\"so_nha\" :\"B2D\",\"ten_duong\" :\"Đông Triều\",\"ten_phuong\" :\"1\",\"ten_quan_huyen\" :\"Quận 1\",\"ten_tinh_thanh\" :\"Hồ Chí Minh\",\"kinh_do\" :\"10\",\"vi_do\" :\"20\",\"chu_thich\" :\"Mắt tiền\",\"so_luot_thich\" :\"30\"}]}";
        AFindingPlace f = new Core.SearchEngine.FindingPlace(token, queryString);
        
        return f.GetPlaces(indexV);
    }

    
}
