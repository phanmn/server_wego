/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.Place;

import BasicStruct.Place;
import Core.RemovingLovedPlace;
import CoreInterface.Place.IAddingLovedPlace;
import CoreInterface.Place.IViewingLovedPlaces;
import JsonTemplate.JsonUtility;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("place/lovedplace")
public class LovedPlaces 
{
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AddingLovedPlace
     */
    public LovedPlaces() {
    }

    @POST
    @Consumes("application/json")
    public String putJson(String content, @QueryParam("token") String token) 
    {
        int maDuLieu = -1;
        content = JsonUtility.StandardlizeJson(content);
        
        try
        {
            Gson gson = new Gson();
            Place p = gson.fromJson(content, Place.class);
            if(p.ma_du_lieu != null)
                maDuLieu = p.ma_du_lieu;
        }
        catch(JsonSyntaxException e)
        {
            System.out.println(e.toString());
        }
        
        IAddingLovedPlace f = new Core.AddingLovedPlace();
        return f.AddLovedPlace(token, maDuLieu);
    }
    
    @GET
    @Produces("application/json; charset=utf-8")
    public String getJson(@QueryParam("token") String token, @QueryParam("index") int index, @DefaultValue("-1") @QueryParam("ma_du_lieu") int maDuLieu) 
    {
        IViewingLovedPlaces f = new Core.ViewingLovedPlace();
        return f.ViewLovedPlaces(token, index, maDuLieu);
    }
    
    @DELETE
    @Produces("application/json; charset=utf-8")
    public String deleteLovePlace(@QueryParam("token") String token, @QueryParam("ma_du_lieu") Integer maDuLieu) 
    {
        RemovingLovedPlace f = new RemovingLovedPlace(token);
        return f.Do(maDuLieu);
    }
}
