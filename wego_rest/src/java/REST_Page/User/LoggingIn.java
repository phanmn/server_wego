/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.User;

import BasicStruct.UserInfo;
import CoreInterface.User.ILoggingIn;
import JsonTemplate.JsonUtility;
import com.google.gson.Gson;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.POST;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("user/login")
public class LoggingIn 
{
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserLogin
     */
    public LoggingIn() {
    }

     
    @POST
    @Consumes("application/json")
    public String getJson(String content) 
    {
        Gson gson = new Gson();
        UserInfo u = null;
        
        content = JsonUtility.StandardlizeJson(content);
        try
        {
            u = gson.fromJson(content, UserInfo.class);
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }
        
        ILoggingIn f = new Core.LoggingIn();
        
        return f.Logging(u);
    }
}
