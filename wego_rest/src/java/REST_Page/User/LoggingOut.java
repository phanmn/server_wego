/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.User;

import CoreAbstract.User.ALoggingOut;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("user/logout")
public class LoggingOut 
{
    @Context
    private UriInfo context;

    public LoggingOut() 
    {
    }

    @DELETE
    @Produces("application/json; charset=utf-8")
    public String getJson(@QueryParam("token") String token) 
    {
        ALoggingOut f = new Core.LoggingOut(token);
        
        return f.LoggingOut();
    }

}
