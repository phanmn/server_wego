/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.User;

import BasicStruct.Feeling;
import CoreInterface.Place.IGettingComment;
import CoreInterface.Place.IGettingFeeling;
import CoreInterface.Place.IPosingFeeling;
import JsonTemplate.JsonUtility;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("place/feeling")
public class PlaceFeeling 
{
    @Context
    private UriInfo context;

    public PlaceFeeling() {
    }

    @GET
    @Produces("application/json")
    public String getJson(@QueryParam("token") String token, @QueryParam("ma_du_lieu") Integer maDuLieu) 
    {
        Feeling f1 = new Feeling();
        f1.ma_du_lieu = maDuLieu;
        IGettingFeeling f = new Core.GettingFeeling();
        
        return f.GetFeeling(token, f1);
    }

    
    @POST
    @Consumes("application/json")
    public String putJson(String content, @QueryParam("token") String token)
    {
        content = JsonUtility.StandardlizeJson(content);
        Feeling f1 = null;
        
        try
        {
            Gson gson = new Gson();
            f1 = gson.fromJson(content, Feeling.class);
        }
        catch(JsonSyntaxException e)
        {
            System.out.println(e.toString());
        }
        
        IPosingFeeling f = new Core.PosingFeeling();
        return f.PostFeeling(token, f1);
    }
}
