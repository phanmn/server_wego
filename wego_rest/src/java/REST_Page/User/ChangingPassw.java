/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.User;

import CoreAbstract.User.AChangingPassw;
import JsonTemplate.JsonUtility;
import com.google.gson.Gson;
import java.util.Hashtable;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("user/changepassw")
public class ChangingPassw {

    @Context
    private UriInfo context;

    public ChangingPassw() {
    }
    
    @POST
    @Consumes("application/json")
    public String putJson(String content, @QueryParam("token") String token) 
    {
        Gson gson = new Gson();
        String oldPassw = null;
        String newPassw = null;
        Hashtable<String, String> h;
        
        // Get Data from json
        content = JsonUtility.StandardlizeJson(content);
        try
        {
            h = gson.fromJson(content, Hashtable.class);
            oldPassw = h.get("mat_khau_cu");
            newPassw = h.get("mat_khau_moi");
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }
        
        AChangingPassw f = new Core.ChangingPassw(token, oldPassw, newPassw);
        
        return f.ChangePassw();
    }
}
