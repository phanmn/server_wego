/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.User;

import BasicStruct.UserInfo;
import CoreInterface.User.IRegistration;
import JsonTemplate.JsonUtility;
import com.google.gson.Gson;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("user/register")
public class Registration {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of Registration
     */
    public Registration() {
    }

    /**
     * PUT method for updating or creating an instance of Registration
     * @param content representation for the resource
     * @return 
     */
    @POST
    @Consumes("application/json")
    public String putJson(String content) 
    {
        Gson gson = new Gson();
        UserInfo u = null;
        IRegistration f = new Core.Registration();
        
        content = JsonUtility.StandardlizeJson(content);
        try
        {
            u = gson.fromJson(content, UserInfo.class);
        }
        catch(Exception e)
        {
            System.out.println(e.toString());
        }
        
        return f.Registry(u);
    }
}
