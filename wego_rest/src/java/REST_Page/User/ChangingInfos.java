/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page.User;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Phan Minh Nhut
 */
@Path("user/changeinfo")
public class ChangingInfos 
{

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ChangingUserInfo
     */
    public ChangingInfos() 
    {
    }
    /**
     * PUT method for updating or creating an instance of ChangingInfos
     * @param content representation for the resource
     * @param token
     */
    @POST
    @Consumes("application/json")
    public void putXml(String content, @DefaultValue("none") @QueryParam("token") String token) 
    {
    }
}
