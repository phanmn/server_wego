/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package REST_Page;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Phan Minh Nhut
 */
@javax.ws.rs.ApplicationPath("")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(REST_Page.Place.Comment.class);
        resources.add(REST_Page.Place.FindingPlace.class);
        resources.add(REST_Page.Place.LovedPlaces.class);
        resources.add(REST_Page.Place.ServiceDetails.class);
        resources.add(REST_Page.User.ChangingInfos.class);
        resources.add(REST_Page.User.ChangingPassw.class);
        resources.add(REST_Page.User.LoggingIn.class);
        resources.add(REST_Page.User.LoggingOut.class);
        resources.add(REST_Page.User.PlaceFeeling.class);
        resources.add(REST_Page.User.Registration.class);
        resources.add(REST_Page.User.ViewingInfo.class);
    }
    
}
