/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreAbstract.Place;

import CoreInterface.Place.IFindingPlace;

/**
 *
 * @author Phan Minh Nhut
 */
abstract public class AFindingPlace implements IFindingPlace
{
    protected String token;
    protected String queryString;

    public AFindingPlace(String token, String queryString) 
    {
        this.token = token;
        this.queryString = queryString;
    }
    
    @Override
    abstract public String GetPlaces(int index);
}
