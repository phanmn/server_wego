/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreAbstract.User;

/**
 *
 * @author Phan Minh Nhut
 */
abstract public class AChangingPassw 
{
    protected String token;
    protected String oldPassw;
    protected String newPassw;

    public AChangingPassw(String token, String oldPassw, String newPassw) 
    {
        this.token = token;
        this.oldPassw = oldPassw;
        this.newPassw = newPassw;
    }
    
    abstract public String ChangePassw();
}
