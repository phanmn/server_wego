/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreAbstract.User;

/**
 *
 * @author Phan Minh Nhut
 */
abstract public class ALoggingOut 
{
    protected String token;
    
    public ALoggingOut(String token)
    {
        this.token = token;
    }
    
    abstract public String LoggingOut();
}
