/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import CoreAbstract.User.ALoggingOut;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;

/**
 *
 * @author Phan Minh Nhut
 */
public class LoggingOut extends ALoggingOut
{
    public LoggingOut(String token) {
        super(token);
    }
    @Override
    public String LoggingOut() 
    {
        Gson gson = new Gson();
        JsonTemplate.JsonSuccess jsonSuc;
        TokenManager tokMan = TokenManager.GetInstance();
        
        if(tokMan.CheckToken(token) && tokMan.RemoveToken(token))
            jsonSuc = new JsonSuccess("true");
        else
            jsonSuc = new JsonSuccess("false");
        
        return gson.toJson(jsonSuc, JsonTemplate.JsonSuccess.class);
    }
}
