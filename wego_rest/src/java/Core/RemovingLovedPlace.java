/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;

/**
 *
 * @author Phan Minh Nhut
 */
public class RemovingLovedPlace 
{
    String token;

    public RemovingLovedPlace(String token) 
    {
        this.token = token;
    }
    
    public String Do(Integer maDuLieu)
    {
        TokenManager tokMan = TokenManager.GetInstance();
        Gson gson = new Gson();
        JsonSuccess oJson;
        
        if(token == null || !tokMan.CheckToken(token))
        {
            oJson = new JsonSuccess(false);
        }
        else
        {
            IRepository dbController = new Repository();
            oJson = new JsonSuccess(dbController.deleteLovedPlace(tokMan.GetValue(token), maDuLieu));
            dbController.closeConnection();
        }
        
        return gson.toJson(oJson);
    }
}
