/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import BasicStruct.UserInfo;
import CoreInterface.User.IRegistration;
import DBController.*;
import JsonTemplate.JsonReason;
import JsonTemplate.JsonToken;
import com.google.gson.Gson;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 *
 * @author Phan Minh Nhut
 */
public class Registration implements IRegistration
{
   @Override
    public String Registry(UserInfo userInfo) 
    {
        Gson gson = new Gson();
        String returnedJson;
        Hashtable<String, Boolean> dic = new  Hashtable<String, Boolean>();
        boolean b;
        
        //
        if(userInfo == null)
        {
            JsonReason oJson = new JsonReason();
            oJson.wrong_field = "ALL";
            oJson.reason = "Format synstax is wrong";
            returnedJson = gson.toJson(oJson);

            return returnedJson;
        }
        
        // Is this username existing
        IRepository dbController = new Repository();
        if(dbController.checkUserNameExist(userInfo.ten_tai_khoan))
        {
            JsonReason oJson = new JsonReason();
            oJson.wrong_field = "ten_tai_khoan";
            oJson.reason = "This username already exists";
            returnedJson = gson.toJson(oJson);
            dbController.closeConnection();
            return returnedJson;
        }
        
        if(dbController.checkEmailExist(userInfo.email))
        {
            JsonReason oJson = new JsonReason();
            oJson.wrong_field = "email";
            oJson.reason = "This email already exists";
            returnedJson = gson.toJson(oJson);
            dbController.closeConnection();
            return returnedJson;
        }
                
        // If not check syntax
        dic.put("Email", userInfo.CheckEmailSyntax());
        dic.put("Ngay sinh", userInfo.CheckBirthDateSyntax());
         
        for (Enumeration e = dic.keys(); e.hasMoreElements();)
        {
            String key = (String)e.nextElement();
            b = dic.get(key);
            if(!b)
            {
                JsonReason oJson = new JsonReason();
                oJson.wrong_field = key;
                oJson.reason = "Wrong Syntax";
                returnedJson = gson.toJson(oJson);
                
                return returnedJson;
            }
        }
               
        // Syntax is ok -> Add new user to database
        if(dbController.registerAccount(userInfo.ten_tai_khoan, userInfo.mat_khau, userInfo.ho_va_ten, userInfo.email, userInfo.ngay_sinh))// Add user to database
        {
            JsonToken oJson = new JsonToken("true");
            oJson.token = TokenManager.GetInstance().MakeToken(userInfo.ten_tai_khoan);
            returnedJson = gson.toJson(oJson);
            dbController.closeConnection();
            return returnedJson;
        }
        
        dbController.closeConnection();
        JsonReason oJson = new JsonReason();
        oJson.wrong_field = "";
        oJson.reason = "Please try again!";
        returnedJson = gson.toJson(oJson);

        return returnedJson;
    }
    
}
