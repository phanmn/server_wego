/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import java.util.Hashtable;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author Phan Minh Nhut
 */
public class TokenManager 
{
    private static TokenManager instance = null;
    private final Map<String, String> tokens = new  Hashtable<String, String>();
    private final Map<String, Date> detailTokens = new Hashtable<String, Date>();
    
    private TokenManager()
    {
        
    }
    
    public static TokenManager GetInstance()
    {
        if(instance == null)
            instance = new TokenManager();
        return instance;
    }
    
    public String MakeToken(String userName)
    {
        String token;
        
        if(!detailTokens.containsKey(userName))
        {
            Date dNow = new Date();
            detailTokens.put(userName, dNow);
            
            token = userName;// Test token - userName
            tokens.put(token, userName); 
        }
        else
        {
            token = tokens.get(userName);
        }
        
        return token; // TESTTT
    }
    
    public boolean CheckToken(String token)
    {
        boolean rv;
        try
        {
            rv = tokens.containsKey(token);
        }catch(Exception e)
        {
            rv = false;
        }
        
        return rv;
    }
    
    public boolean RemoveToken(String token)
    {
        boolean rv = true;
        
        try
        {
            tokens.remove(token);
        }
        catch(Exception e)
        {
            rv = false;
        }
        
        return rv;
    }

    public String GetValue(String token)
    {
        return tokens.get(token);
    }
}
