/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core.SearchEngine;

import BasicStruct.Place;
import Core.TokenManager;
import CoreAbstract.Place.AFindingPlace;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonPlace;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FindingPlace extends AFindingPlace 
{
    public FindingPlace(String token, String queryString) 
    {
        super(token, queryString);
    }
    
    @Override
    public String GetPlaces(int index) 
    {
        Gson gson = new GsonBuilder().serializeNulls().create();
        TokenManager tokMan = TokenManager.GetInstance();
        
        if(token == null || !tokMan.CheckToken(token) || queryString == null || queryString.length() == 0)
        {
            JsonSuccess oJson = new JsonSuccess("false");
            return gson.toJson(oJson);
        }
        
        // tukhoatinhthanh -> tukhoaquanhuyen -> tukhoaphuong -> tukhoaduong -> tukhoadiadiem -> tukhoadichvu
        IRepository dbController = new Repository();
        Map<String, String> m = new HashMap<String, String>();
        m.put("tukhoatinhthanh", "");
        m.put("tukhoaquanhuyen", "");
        m.put("tukhoaphuong", "");
        m.put("tukhoadiadiem", "");
        m.put("tukhoadichvu", "");
        m.put("tukhoaduong", "");
        String[] arr = {"tukhoatinhthanh", "tukhoaquanhuyen", "tukhoaphuong", "tukhoaduong", "tukhoadichvu", "tukhoadiadiem"};
        
        boolean flagSearch = false;
        
        for(int i = 0; i < arr.length; i++)
        {
            String vM = m.get(arr[i]);
            String kM = arr[i];
            if(queryString.length() < 2)
               break;
           Map<Integer, String> rv = dbController.searchKeyword((String) kM, queryString); 
           int count = 0;
           for (Map.Entry entry_2 : rv.entrySet())
           {
               String oldV = vM;
               Integer v = (Integer) entry_2.getKey();
               String sV = v.toString();
               if(count != 0)
               {
                   sV = oldV + ',' + v.toString();
               }
               count = 1;
            vM = sV;
            flagSearch = true;
            m.put((String) kM, sV);
            String s = (String) entry_2.getValue();
            queryString = queryString.replaceAll(s, "");
           }
        }
        
        if(flagSearch)
        {
            ArrayList<Place> arrPlace;
            arrPlace = dbController.searchPlaces(m, index);
            dbController.closeConnection();
            if(arrPlace.size() > 0)
            {
                JsonPlace oJson = new JsonPlace("true");
                oJson.SetPlaceContent(arrPlace);
                return gson.toJson(oJson);
            }
        }
        JsonSuccess oJson = new JsonSuccess("false");
        return gson.toJson(oJson);
    }
    
}
