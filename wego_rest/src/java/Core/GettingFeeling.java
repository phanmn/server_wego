/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import BasicStruct.Feeling;
import CoreInterface.Place.IGettingFeeling;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonFeeling;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;

/**
 *
 * @author Phan Minh Nhut
 */
public class GettingFeeling implements IGettingFeeling
{
    @Override
    public String GetFeeling(String token, Feeling f) 
    {
        TokenManager tokMan = TokenManager.GetInstance();
        Gson gson = new Gson();
        if(f == null || f.ma_du_lieu == null || token == null || !tokMan.CheckToken(token))
        {
            JsonSuccess oJson = new JsonSuccess(false);
            return gson.toJson(oJson);
        }
        
        IRepository dbController = new Repository();
        String userName = tokMan.GetValue(token);
        boolean b = dbController.getFeeling(userName, f.ma_du_lieu);
        dbController.closeConnection();
        
        JsonFeeling oJson = new JsonFeeling(true);
        f.thich = b;
        oJson.SetContent(f);
        return gson.toJson(oJson);
    }
}
