/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import CoreInterface.Place.IAddingLovedPlace;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;

/**
 *
 * @author Phan Minh Nhut
 */
public class AddingLovedPlace implements IAddingLovedPlace
{
    @Override
    public String AddLovedPlace(String token, int maDuLieu) 
    {
        TokenManager tokMan = TokenManager.GetInstance();
        boolean rv;
        
        if(rv = (tokMan.CheckToken(token) && maDuLieu > 0) )
        {
            IRepository dbController = new Repository();
            rv = dbController.addLovedPlace(tokMan.GetValue(token), maDuLieu);
            dbController.closeConnection();
        }
        JsonSuccess oJson = new JsonSuccess(rv);
        
        Gson gson = new Gson();
        return gson.toJson(oJson);
    }
}
