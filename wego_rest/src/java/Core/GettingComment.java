/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import BasicStruct.DetailsComment;
import DBController.IRepository;
import DBController.Repository;
import CoreInterface.Place.IGettingComment;
import JsonTemplate.JsonComment;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author Phan Minh Nhut
 */
public class GettingComment implements IGettingComment
{
    String token;

    public GettingComment(String token) 
    {
        this.token = token;
    }
    
    @Override
    public String GetComment(int maDuLieu, int indexV)
    {
       TokenManager tokMan = TokenManager.GetInstance();
       String retuJson;
       Gson gson = new Gson();
       
       if(maDuLieu == 0 || indexV < 1 || token == null || !tokMan.CheckToken(token)) 
       {
           JsonSuccess oJson;
           oJson = new JsonSuccess("false");
           retuJson = gson.toJson(oJson);
       }
       else
       {
           IRepository dbController = new Repository();
           ArrayList<DetailsComment> arComment = dbController.getUserComments(maDuLieu, indexV);
           JsonComment oJson;
           
           oJson = new JsonComment("true");
           oJson.SetCommentContent(arComment);
           
           retuJson = gson.toJson(oJson);
           dbController.closeConnection();
       }
       
       return retuJson;
    }
}
