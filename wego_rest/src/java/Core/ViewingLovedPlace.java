/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import BasicStruct.Place;
import CoreInterface.Place.IViewingLovedPlaces;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonPlace;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;

/**
 *
 * @author Phan Minh Nhut
 */
public class ViewingLovedPlace implements IViewingLovedPlaces
{
    @Override
    public String ViewLovedPlaces(String token, int index, int maDuLieu) 
    {
        TokenManager tokMan = TokenManager.GetInstance();
        Gson gson = new GsonBuilder().serializeNulls().create();
        if(!tokMan.CheckToken(token))
        {
            JsonSuccess oJson = new JsonSuccess(false);
            return gson.toJson(oJson);
        }
        
        IRepository dbController = new Repository();
        ArrayList<Place> lovedPlaces = dbController.GetLovedPlaces(tokMan.GetValue(token), index, maDuLieu);
        dbController.closeConnection();
        
        JsonPlace oJson = new JsonPlace(true);
        oJson.SetPlaceContent(lovedPlaces);
        return gson.toJson(oJson); 
    }
    
}
