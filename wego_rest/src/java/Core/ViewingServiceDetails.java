/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import BasicStruct.ServiceDetails;
import CoreInterface.Place.IViewingServiceDetails;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonService;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;

/**
 *
 * @author Phan Minh Nhut
 */
public class ViewingServiceDetails implements IViewingServiceDetails
{
    @Override
    public String GetServiceDetails(String token, int maDuLieu)
    {
        TokenManager tokMan = TokenManager.GetInstance();
        Gson gson = new GsonBuilder().serializeNulls().create();
        if(!tokMan.CheckToken(token))
        {
            JsonSuccess oJson = new JsonSuccess(false);
            return gson.toJson(oJson);
        }
        
        IRepository dbController = new Repository();
        ArrayList<ServiceDetails> servDetails = dbController.getServiceDetails(maDuLieu);
        dbController.closeConnection();
        
        JsonService oJson = new JsonService(true);
        oJson.SetPlaceContent(servDetails);
        return gson.toJson(oJson); 
    }
}
