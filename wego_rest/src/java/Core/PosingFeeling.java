/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import BasicStruct.Feeling;
import CoreInterface.Place.IPosingFeeling;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;

/**
 *
 * @author Phan Minh Nhut
 */
public class PosingFeeling implements IPosingFeeling
{
    @Override
    public String PostFeeling(String token, Feeling f) 
    {
        TokenManager tokMan = TokenManager.GetInstance();
        Gson gson = new Gson();
        boolean b = false;
        if(f == null || f.ma_du_lieu == null || f.thich == null || token == null || !tokMan.CheckToken(token))
        {}
        else
        {
            IRepository dbController = new Repository();
            b = dbController.setFeeling(tokMan.GetValue(token), f.ma_du_lieu, f.thich);
            dbController.closeConnection();
        }
        
        JsonSuccess oJson = new JsonSuccess(b);
        return gson.toJson(oJson);
    }
    
}
