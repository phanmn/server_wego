/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import BasicStruct.DetailsComment;
import DBController.IRepository;
import DBController.Repository;
import CoreInterface.Place.IPostingComment;
import JsonTemplate.JsonComment;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;
import java.util.ArrayList;

/**
 *
 * @author Phan Minh Nhut
 */
public class PostingComment implements IPostingComment
{
    String token;
    public PostingComment(String token)
    {
        this.token = token;
    }
    
    @Override
    public String PostComment(DetailsComment c)
    {
        String retuJson;
        Gson gson = new Gson();
        TokenManager tokMan = TokenManager.GetInstance();
        
        if(c == null || c.ma_du_lieu == null || c.comment == null || token == null || !tokMan.CheckToken(token))
        {
            JsonSuccess oJson = new JsonSuccess("false");
            retuJson =  gson.toJson(oJson);
        }
        else
        {
            String userName = tokMan.GetValue(token);
            IRepository dbController = new Repository();

            ArrayList<DetailsComment> arComment = dbController.addUserComment(c.ma_du_lieu, userName, c.comment);
            JsonComment oJson;

            oJson = new JsonComment("true");
            oJson.SetCommentContent(arComment);

            retuJson = gson.toJson(oJson);
            dbController.closeConnection();
        }
        
        return retuJson;
    }
}
