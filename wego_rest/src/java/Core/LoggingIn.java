/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import CoreInterface.User.ILoggingIn;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonSuccess;
import JsonTemplate.JsonToken;
import com.google.gson.Gson;

/**
 *
 * @author Phan Minh Nhut
 */
public class LoggingIn implements ILoggingIn
{
    @Override
    public String Logging(BasicStruct.UserInfo u) 
    {
        Gson gson = new Gson();
        String returnedJson;
        
        IRepository dbController = new Repository();
        if(u == null || u.ten_tai_khoan == null || u.mat_khau == null || !dbController.userLoGin(u.ten_tai_khoan, u.mat_khau))
        {
            dbController.closeConnection();
            JsonSuccess json = new JsonSuccess("false");
            returnedJson = gson.toJson(json);
            return returnedJson;
        }
        
        dbController.closeConnection();
        JsonToken oJson = new JsonToken("true");
        oJson.token = TokenManager.GetInstance().MakeToken(u.ten_tai_khoan);
        returnedJson = gson.toJson(oJson);
        return returnedJson;
    }
    
}
