/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Core;

import CoreAbstract.User.AChangingPassw;
import DBController.IRepository;
import DBController.Repository;
import JsonTemplate.JsonSuccess;
import com.google.gson.Gson;

/**
 *
 * @author Phan Minh Nhut
 */
public class ChangingPassw extends AChangingPassw
{
    public ChangingPassw(String token, String oldPassw, String newPassw) 
    {
        super(token, oldPassw, newPassw);
    }

    @Override
    public String ChangePassw() 
    {
        Gson gson = new Gson();
        JsonSuccess jsonO;
        
        jsonO = new JsonSuccess(ValidatePassws() && UpdateDB());
       
        return gson.toJson(jsonO);
    }
    
    private boolean ValidateSyntaxPassw(String passw) 
    {
       return passw != null && passw.length() > 5 && passw.indexOf(" ") < 0;
    }
    
     private boolean ValidatePassws()
    {
        return ValidateSyntaxPassw(oldPassw) && ValidateSyntaxPassw(newPassw) && !oldPassw.equals(newPassw);
    }

    private boolean UpdateDB()
    {
        TokenManager tokMan = TokenManager.GetInstance();
        IRepository dbController = new Repository();
        
        boolean rv = tokMan.CheckToken(token) && dbController.updatePassw(tokMan.GetValue(token), oldPassw, newPassw);
        dbController.closeConnection();
            
        return rv;
    }
}
