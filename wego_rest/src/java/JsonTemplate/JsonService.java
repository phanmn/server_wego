/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JsonTemplate;

import BasicStruct.ServiceDetails;
import java.util.ArrayList;

/**
 *
 * @author Phan Minh Nhut
 */
public class JsonService extends JsonSuccess
{
    private ArrayList<ServiceDetails> content = new ArrayList<ServiceDetails>();
    public JsonService(String success) 
    {
        super(success);
    }
    public JsonService(boolean success) 
    {
        super(success);
    }
    
    public void AddService(ServiceDetails s)
    {
        content.add(s);
    }
    public void SetPlaceContent(ArrayList<ServiceDetails> arService)
    {
        content = arService;
    }
}
