/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JsonTemplate;

/**
 *
 * @author Phan Minh Nhut
 */
public class JsonReason extends JsonSuccess
{
    public String wrong_field = "";
    public String reason = "";

    public JsonReason() {
        super("false");
    }
}
/*
    “success” : “false”
    “wrong_field” : “mat_khau_cu”,
    “reason” : “Sai mật khẩu”
*/