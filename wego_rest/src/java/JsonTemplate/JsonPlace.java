/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JsonTemplate;

import BasicStruct.Place;
import java.util.ArrayList;

/**
 *
 * @author Phan Minh Nhut
 */
public class JsonPlace extends JsonSuccess
{
    private ArrayList<Place> content = new ArrayList<Place>();
    public JsonPlace(String success) 
    {
        super(success);
    }
    public JsonPlace(boolean success) 
    {
        super(success);
    }
    
    public void AddPlace(Place p)
    {
        content.add(p);
    }
    public void SetPlaceContent(ArrayList<Place> arPlace)
    {
        content = arPlace;
    }
}
