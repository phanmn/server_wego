/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JsonTemplate;

import BasicStruct.DetailsComment;
import BasicStruct.Place;
import java.util.ArrayList;

/**
 *
 * @author Phan Minh Nhut
 */
public class JsonComment extends JsonSuccess
{
    private ArrayList<DetailsComment> content = new ArrayList<DetailsComment>();
    public JsonComment(String success) 
    {
        super(success);
    }
    
    public void AddComment(DetailsComment p)
    {
        content.add(p);
    }
    public void SetCommentContent(ArrayList<DetailsComment> arComment)
    {
        content = arComment;
    }
}
