/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package JsonTemplate;

import BasicStruct.Feeling;

/**
 *
 * @author Phan Minh Nhut
 */
public class JsonFeeling extends JsonSuccess
{
    Feeling content;
    public JsonFeeling(String success) {
        super(success);
    }
     public JsonFeeling(boolean success) {
        super(success);
    }
     
    public void SetContent(Feeling f)
    {
        content = f;
    }
}
