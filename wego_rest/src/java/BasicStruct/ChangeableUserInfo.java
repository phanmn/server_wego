/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BasicStruct;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.commons.validator.routines.EmailValidator;
/**
 *
 * @author Phan Minh Nhut
 */
public class ChangeableUserInfo 
{
    public String ho_va_ten = null;
    public String email = null;
    public String ngay_sinh = null;
    
    public boolean CheckEmailSyntax()
    {
        return EmailValidator.getInstance().isValid(email);
    }
    
    public boolean CheckBirthDateSyntax()
    {
        boolean rv = true;
        
        SimpleDateFormat objDateFormat = new SimpleDateFormat("y-M-d");
        
        try
        {
            objDateFormat.parse(ngay_sinh);
        }catch(ParseException e)
        {
            rv = false;
        }
                
        return rv;
    }

}
