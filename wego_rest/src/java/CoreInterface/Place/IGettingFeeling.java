/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package CoreInterface.Place;

import BasicStruct.Feeling;

/**
 *
 * @author Phan Minh Nhut
 */
public interface IGettingFeeling 
{
    String GetFeeling(String token, Feeling f);
}
