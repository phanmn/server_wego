DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `searchDulieu`(IN matinhthanh_Var nvarchar(32), IN maquanhuyen_Var nvarchar(32), IN maphuong_Var nvarchar(32), IN maduong_Var nvarchar(32), IN matendiadiem_Var nvarchar(32), IN madichvu_Var nvarchar(32)c
BEGIN
	select MaDuLieu, SoNha, KinhDo, ViDo, ChuThich, SoLuotThich, TenTinhThanh, TenDichVu, TenDiaDiem, TenPhuong, TenDuong, TenQuanHuyen
	
	From DuLieu, DichVu, Phuong, QuanHuyen, TinhThanh, Duong, TenDiaDiem 
	
	Where
		DuLieu.MaDuLieu > fromIndex
		AND
		DuLieu.MaDichVu = DichVu.MaDichVu
		AND
		--DuLieu.MaDichVu = if(madichvu_Var = 0, DuLieu.MaDichVu, madichvu_Var)
		FIND_IN_SET(CAST(DuLieu.MaDichVu as char(10)), if(CHAR_LENGTH(madichvu_Var) > 0, madichvu_Var, CAST(DuLieu.MaDichVu as char(10)))) >= 1
		And
		DuLieu.matendiadiem = TenDiaDiem.matendiadiem
		AND
		-- DuLieu.matendiadiem = if(matendiadiem_Var = 0, DuLieu.matendiadiem, matendiadiem_Var)
		FIND_IN_SET(CAST(DuLieu.matendiadiem as char(10)), if(CHAR_LENGTH(matendiadiem_Var) > 0, matendiadiem_Var, CAST(DuLieu.matendiadiem as char(10)))) >= 1
		And
		DuLieu.maduong = Duong.maduong
		AND
		--DuLieu.maduong = if(maduong_Var = 0, DuLieu.maduong, maduong_Var)
		FIND_IN_SET(CAST(DuLieu.maduong as char(10)), if(CHAR_LENGTH(maduong_Var) > 0, maduong_Var, CAST(DuLieu.maduong as char(10)))) >= 1
		And
		DuLieu.maphuong = Phuong.maphuong
		AND
		--DuLieu.maphuong = if(maphuong_Var = 0, DuLieu.maphuong, maphuong_Var)
		FIND_IN_SET(CAST(DuLieu.maphuong as char(10)), if(CHAR_LENGTH(maphuong_Var) > 0, maphuong_Var, CAST(DuLieu.maphuong as char(10)))) >= 1
		AND
		DuLieu.maquanhuyen = QuanHuyen.maquanhuyen
		AND
		--DuLieu.maquanhuyen = if(maquanhuyen_Var = 0, DuLieu.maquanhuyen, maquanhuyen_Var)
		FIND_IN_SET(CAST(DuLieu.maquanhuyen as char(10)), if(CHAR_LENGTH(maquanhuyen_Var) > 0, maquanhuyen_Var, CAST(DuLieu.maquanhuyen as char(10)))) >= 1
		AND
		DuLieu.matinhthanh = TinhThanh.matinhthanh
		AND
		-- DuLieu.matinhthanh = if(matinhthanh_Var = 0, DuLieu.matinhthanh, matinhthanh_Var)
		FIND_IN_SET(CAST(DuLieu.matinhthanh as char(10)), if(CHAR_LENGTH(matinhthanh_Var) > 0, matinhthanh_Var, CAST(DuLieu.matinhthanh as char(10)))) >= 1
		order by DuLieu.MaDuLieu asc
	LIMIT 20;
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `updatePassw`(IN userName nvarchar(64), IN oldPassw nvarchar(64), IN newPassw nvarchar(64))
BEGIN
	update taikhoan
	where TenTaiKhoan = userName and MatKhau = oldPassw
	set MatKhau = newPassw
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `viewLovedPlaces`(IN userName nvarchar(64), IN currentPage int, IN maDuLieuVar int)
BEGIN
	declare indexStart int default 0;
	set indexStart = (currentPage - 1) * 20;
	
	select MaDuLieu, SoNha, KinhDo, ViDo, ChuThich, SoLuotThich, TenTinhThanh, TenDichVu, TenDiaDiem, TenPhuong, TenDuong, TenQuanHuyen
	From DuLieu, DichVu, Phuong, QuanHuyen, TinhThanh, Duong, TenDiaDiem
	Where
		DuLieu.MaDuLieu = if (maDuLieuVar < 1, DuLieu.MaDuLieu, maDuLieuVar)
		AND
		DuLieu.MaDuLieu IN
						(   
							Select DLYT.MaDuLieu
							from TaiKhoan as TK, DuLieu_YeuThich as DLYT
							where TK.TenTaiKhoan=userName and TK.MaTaiKhoan = DLYT.MaTaiKhoan
						)
		AND
		DuLieu.MaDichVu = DichVu.MaDichVu
		And
		DuLieu.matendiadiem = TenDiaDiem.matendiadiem		
		And
		DuLieu.maduong = Duong.maduong
		And
		DuLieu.maphuong = Phuong.maphuong
		AND
		DuLieu.maquanhuyen = QuanHuyen.maquanhuyen
		AND
		DuLieu.matinhthanh = TinhThanh.matinhthanh
		order by DuLieu.MaDuLieu asc
	LIMIT indexStart, 20;
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `addLovedPlace`(IN userName nvarchar(64), IN MaDuLieu int)
BEGIN
	
	DECLARE accountId int;
	
	Select mataikhoan into accountId
	from taikhoan
	where TenTaiKhoan = userName;
	
	if accountId is not null
	then
		INSERT INTO `wego_rest_db`.`DuLieu_YeuThich`(`MaDuLieu`, `MaTaiKhoan`)
		VALUES( MaDuLieu, MaTaiKhoan);
	end if;
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `getServiceDetails`(IN maDuLieuVar int)
BEGIN
	Select Ten, GiaTien, ChuThich
	From ChiTietDichVu as CTDV
	Where CTDV.MaChiTiet in 
	(
		Select MaChiTiet
		From DuLieu as DL, ChiTiet_DuLieu as CTDL
		Where DL.MaDuLieu = maDuLieuVar and CTDL.MaDuLieu = DL.MaDuLieu
	);
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `getFeeling`(IN userName nvarchar(64), IN maDuLieuVar int, Out feeling int)
BEGIN
	Select count(*) into feeling
	From camxuc_dulieu as cx
	Where cx.madulieu = maDuLieuVar 
	and cx.mataikhoan IN
						(   
							Select MaTaiKhoan
							from TaiKhoan as TK
							where TK.TenTaiKhoan=userName
						);
	
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `getFeelingMTK`(IN maTaiKhoanVar int, IN maDuLieuVar int, Out feeling int)
BEGIN
	Select count(*) into feeling
	From camxuc_dulieu as cx
	Where cx.madulieu = maDuLieuVar and cx.mataikhoan = maTaiKhoanVar;
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `setFeeling`(IN userName nvarchar(64), IN maDuLieuVar int, IN feeling int)
BEGIN
	DECLARE accountId int;
	Declare rowCount int;
	Select mataikhoan into accountId
	from taikhoan
	where TenTaiKhoan = userName;
	
	if accountId is not null
	then
		if feeling = 1
		then
			start transaction;
			INSERT INTO `wego_rest_db`.`camxuc_dulieu`(`MaDuLieu`, `MaTaiKhoan`)
			VALUES( maDuLieuVar, accountId);
			Select ROW_COUNT() into rowCount;
			if(rowCount > 0) then
				update dulieu
				set SoLuotThich = SoLuotThich + 1
				where dulieu.madulieu = maDuLieuVar;
			end if;
			commit;
			select rowCount;
		else
			start transaction;
			delete 
			from camxuc_dulieu
			where MaTaiKhoan = accountId and MaDuLieu = maDuLieuVar;
			Select ROW_COUNT() into rowCount;
			if(rowCount > 0) then
				update dulieu
				set SoLuotThich = SoLuotThich - 1
				where dulieu.madulieu = maDuLieuVar;
			end if;
			commit;
			select rowCount;
		end if;
	end if;
end//
DELIMITER ;

DELIMITER //
CREATE DEFINER=`viemde`@`%` PROCEDURE `getUserComments`(locationId int, currentPage int)
BEGIN
	declare indexStart int default 0;
	set indexStart = (currentPage - 1) * 20;
	select binhluan.*, taikhoan.tentaikhoan
	from binhluan, taikhoan
	where binhluan.mataikhoan = taikhoan.mataikhoan and binhluan.MaDuLieu = locationId
	order by binhluan.thoigian desc
	limit indexStart, 20;
END
DELIMITER ;