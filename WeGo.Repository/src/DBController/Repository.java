package DBController;


import BasicStruct.DetailsComment;
import BasicStruct.Place;
import BasicStruct.ServiceDetails;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Long
 */
public class Repository implements IRepository{
    private Connection connection;
    public Repository()
    {
        try {
		Class.forName("com.mysql.jdbc.Driver");
              
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	}

	connection = null;
 
	try {
		connection = DriverManager
		.getConnection("jdbc:mysql://ec2-50-19-213-178.compute-1.amazonaws.com:3306/wego_rest_db?zeroDateTimeBehavior=convertToNull","viemde", "nctnocky");
               
	} catch (SQLException e) {
		System.out.println("Connection Failed! Check output console");
		e.printStackTrace();

	}
    }
    
    @Override
    public boolean userLoGin(String userName, String passWord) {
        CallableStatement cStmt = null;
        try {
            int count;
            cStmt = connection.prepareCall("{call Login(?,?)}");
            cStmt.setNString("userName", userName);
            cStmt.setNString("passWord", passWord);
            
            ResultSet result = cStmt.executeQuery();
           
            boolean isValid = false;
            while(result.next()){
                isValid = true;
                break;
            }
            result.close();
            cStmt.close();
            return isValid;
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean registerAccount(String userName, String passWord, String fullName, String email, String dateOfBirth) {
        CallableStatement cStmt = null;
        try {      
            cStmt = connection.prepareCall("{call Register(?,?,?,?,?)}");
            cStmt.setNString("userName", userName);
            cStmt.setNString("passWord", passWord);
            cStmt.setNString("fullName", fullName);
            cStmt.setNString("email", email);
//            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
//            java.util.Date date = formatter.parse(dateOfBirth);
            Date sqlDate = Date.valueOf(dateOfBirth) ;
            cStmt.setDate("dateOfBirth", sqlDate);
            //cStmt.setNString("dateOfBirth", dateOfBirth);
            int rowEffect = cStmt.executeUpdate();
            
            cStmt.close();
            
            return rowEffect == 1;
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean checkUserNameExist(String userName) {
        CallableStatement cStmt = null;
        try {
            cStmt = connection.prepareCall("{call checkUserNameExist(?,?)}");
            cStmt.setNString("userName", userName);
            cStmt.registerOutParameter("count", Types.INTEGER);
            cStmt.execute();
            int count = cStmt.getInt("count");
            cStmt.close();
            
            return count == 1;        
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean checkEmailExist(String email) {
        CallableStatement cStmt = null;
        try {
            cStmt = connection.prepareCall("{call checkEmailExist(?,?)}");
            cStmt.setNString("email", email);
            cStmt.registerOutParameter("count", Types.INTEGER);
            cStmt.execute();
            int count = cStmt.getInt("count");
            cStmt.close();
            
            return count == 1;
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public Map<Integer, String> searchKeyword(String tableName, String keyWord) 
    {
        Map<Integer, String> rv = new HashMap<>();
        CallableStatement cStmt = null;
        try {
            cStmt = connection.prepareCall("{call searchTableWithKey(?,?)}");
            cStmt.setNString("tableName", tableName);
            cStmt.setNString("tukhoa", keyWord);
            ResultSet rs = cStmt.executeQuery();
            
            while(rs.next())
            {
                rv.put(rs.getInt("id"), rs.getString("tukhoadung"));
            }
            
            rs.close();
            cStmt.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rv;
    }
    
    @Override
    public void closeConnection(){
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
   @Override
   public ArrayList<Place> searchPlaces(Map<String, String> m, int index)
   {
       ArrayList<Place> rv = new ArrayList<>();
       CallableStatement cStmt;
       try 
       {
            cStmt = connection.prepareCall("{call searchDulieu(?,?,?,?,?,?,?)}");

            for (Map.Entry entry : m.entrySet())
            {
                String k = (String) entry.getKey();
                String v = (String)entry.getValue();
                
                if(k.indexOf("tukhoadiadiem") >= 0)
                    k = k.replaceAll("tukhoa", "maten");
                else
                    k = k.replaceAll("tukhoa", "ma");
                k = k + "_Var";
                cStmt.setString(k, v);
            }
            cStmt.setInt("fromIndex", index);
            
            ResultSet rs = cStmt.executeQuery();
            while(rs.next())
            {
                Place p = new Place();
                p.chu_thich = rs.getString("chuthich");
                p.kinh_do = rs.getFloat("kinhdo");
                p.vi_do = rs.getFloat("vido");
                p.ma_du_lieu = rs.getInt("madulieu");
                p.so_luot_thich = rs.getInt("soluotthich");
                p.so_nha = rs.getString("sonha");
                p.ten_dia_diem = rs.getString("tendiadiem");
                p.ten_dich_vu = rs.getString("tendichvu");
                p.ten_duong = rs.getString("tenduong");
                p.ten_quan_huyen = rs.getString("tenquanhuyen");
                p.ten_tinh_thanh = rs.getString("tentinhthanh");
                p.ten_phuong = rs.getString("tenphuong");
                rv.add(p);
            }

            rs.close();
            cStmt.close();
        } catch (SQLException ex) 
        {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rv;
   }

    @Override
    public ArrayList<DetailsComment> addUserComment(int locationId, String userName, String commentContent) 
    {
        CallableStatement cStmt;
        ArrayList<DetailsComment> rv = new ArrayList<>();
        try {
            cStmt = connection.prepareCall("{call addUserComment(?,?,?)}");
            cStmt.setString("userName", userName);
            cStmt.setInt("locationId", locationId);
            cStmt.setNString("commentContent", commentContent);
            
            ResultSet rs = cStmt.executeQuery();
            while(rs.next())
            {
                DetailsComment comment = new DetailsComment();
                comment.ma_binh_luan = rs.getInt("MaBinhLuan");
                comment.ma_du_lieu = rs.getInt("MaDuLieu");
                comment.comment = rs.getNString("NoiDung");
                comment.thoi_gian = rs.getString("ThoiGian");
                comment.ten_tai_khoan = rs.getNString("TenTaiKhoan");
                rv.add(comment);
            }

             rs.close();
             cStmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rv;
    }

    @Override
    public ArrayList<DetailsComment> getUserComments(int locationId, int currentPage) {
        ArrayList<DetailsComment> rv = new ArrayList<>();
        CallableStatement cStmt;
        try 
        {
             cStmt = connection.prepareCall("{call getUserComments(?,?)}");
             cStmt.setInt("locationId", locationId);
             cStmt.setInt("currentPage", currentPage);

             ResultSet rs = cStmt.executeQuery();
             while(rs.next())
             {
                 DetailsComment comment = new DetailsComment();
                 comment.ma_binh_luan = rs.getInt("MaBinhLuan");
                 comment.ma_du_lieu = rs.getInt("MaDuLieu");
                 comment.comment = rs.getNString("NoiDung");
                 comment.thoi_gian = rs.getString("ThoiGian");
                 comment.ten_tai_khoan = rs.getNString("TenTaiKhoan");
                 rv.add(comment);
             }

             rs.close();
             cStmt.close();
         } catch (SQLException ex) 
         {
             Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
         }
         return rv;
    }
    
    @Override
    public boolean updatePassw(String userName, String oldPassw, String newPassw)
    {
        CallableStatement cStmt;
        try 
        {      
            cStmt = connection.prepareCall("{call updatePassw(?,?,?)}");
            cStmt.setNString("userName", userName);
            cStmt.setNString("oldPassw", oldPassw);
            cStmt.setNString("newPassw", newPassw);
            
            int rowEffect = cStmt.executeUpdate();
            cStmt.close();
            
            return rowEffect == 1;
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
       
    }

    @Override
    public ArrayList<Place> GetLovedPlaces(String userName, int index, int maDuLieu)
    {
        CallableStatement cStmt;
        ArrayList<Place> rv = new ArrayList<>();
        
        try 
        {      
            cStmt = connection.prepareCall("{call viewLovedPlaces(?,?,?)}");
            cStmt.setNString("userName", userName);
            cStmt.setInt("currentPage", index);
            cStmt.setInt("maDuLieuVar", maDuLieu);
            
            ResultSet rs = cStmt.executeQuery();
            while(rs.next())
            {
                Place p = new Place();
                p.chu_thich = rs.getString("chuthich");
                p.kinh_do = rs.getFloat("kinhdo");
                p.vi_do = rs.getFloat("vido");
                p.ma_du_lieu = rs.getInt("madulieu");
                p.so_luot_thich = rs.getInt("soluotthich");
                p.so_nha = rs.getString("sonha");
                p.ten_dia_diem = rs.getString("tendiadiem");
                p.ten_dich_vu = rs.getString("tendichvu");
                p.ten_duong = rs.getString("tenduong");
                p.ten_quan_huyen = rs.getString("tenquanhuyen");
                p.ten_tinh_thanh = rs.getString("tentinhthanh");
                p.ten_phuong = rs.getString("tenphuong");
                rv.add(p);
            }

            rs.close();
            cStmt.close();
        } catch (SQLException ex) 
        {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rv;
    }

    @Override
    public boolean addLovedPlace(String userName, int maDuLieu)
    {
        CallableStatement cStmt = null;
        try {
            cStmt = connection.prepareCall("{call addLovedPlace(?,?)}");
            cStmt.setString("userName", userName);
            cStmt.setInt("MaDuLieu", maDuLieu);
            
            int rowEffect = cStmt.executeUpdate();
            cStmt.close();
            if(rowEffect > 0) {
                return true;
            }
            else {
                return false;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    @Override
    public ArrayList<ServiceDetails> getServiceDetails(int maDuLieu)
    {
        CallableStatement cStmt;
        ArrayList<ServiceDetails> rv = new ArrayList<>();
        
        try 
        {      
            cStmt = connection.prepareCall("{call getServiceDetails(?)}");
            cStmt.setInt("maDuLieuVar", maDuLieu);
            
            ResultSet rs = cStmt.executeQuery();
            while(rs.next())
            {
                ServiceDetails dService = new ServiceDetails();
                dService.ten = rs.getString("ten");
                dService.gia_tien = rs.getInt("giatien");
                dService.chu_thich = rs.getString("chuthich");
                rv.add(dService);
            }

            rs.close();
            cStmt.close();
        } catch (SQLException ex) 
        {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return rv;
    }

    @Override
    public boolean getFeeling(String userName, int maDuLieu)
    {
        CallableStatement cStmt;
        
        try 
        {      
            cStmt = connection.prepareCall("{call getFeeling(?,?,?)}");
            cStmt.setInt("maDuLieuVar", maDuLieu);
            cStmt.setString("userName", userName);
            cStmt.registerOutParameter("feeling", Types.INTEGER);
            
            cStmt.execute();
            int feeling = cStmt.getInt("feeling");
            cStmt.close();
            
            return feeling == 1;
        } catch (SQLException ex) 
        {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    @Override
    public boolean setFeeling(String userName, int maDuLieu, boolean feeling)
    {
        CallableStatement cStmt;
        try 
        {      
            cStmt = connection.prepareCall("{call setFeeling(?,?,?)}");
            cStmt.setString("userName", userName);
            cStmt.setInt("maDuLieuVar", maDuLieu);
            cStmt.setInt("feeling", feeling ? 1:0);
            
            ResultSet rs = cStmt.executeQuery();
            int count = 0;
            if(rs.next())
                count = rs.getInt("rowCount");
            
            rs.close();
            cStmt.close();
            
            return count == 1;
        } 
        catch(MySQLIntegrityConstraintViolationException ex)
        {
            return false;
        }
        catch (SQLException ex) 
        {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    @Override
    public boolean deleteLovedPlace(String userName, int maDuLieu) {
         CallableStatement cStmt = null;
        try {
            cStmt = connection.prepareCall("{call deleteLovedPlace(?,?)}");
            cStmt.setString("userName", userName);
            cStmt.setInt("MaDuLieu", maDuLieu);
            
            int rowEffect = cStmt.executeUpdate();
            cStmt.close();
            if(rowEffect > 0) {
                return true;
            }
            else {
                return false;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Repository.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}