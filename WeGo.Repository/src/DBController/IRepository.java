package DBController;

import BasicStruct.DetailsComment;
import BasicStruct.Place;
import BasicStruct.ServiceDetails;
import java.util.ArrayList;
import java.util.Map;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Long
 */
public interface IRepository {
    public boolean userLoGin(String userName, String passWord);
    public boolean updatePassw(String userName, String oldPassw, String newPassw);
    public boolean registerAccount (String userName, String passWord, String fullName, String email, String dateOfBirth);
    public boolean checkUserNameExist (String userName);
    public boolean checkEmailExist (String email);
    public Map<Integer, String> searchKeyword(String tableName, String keyWord);
    public void closeConnection();
    public ArrayList<Place> searchPlaces(Map<String, String> m, int index);
    public ArrayList<DetailsComment> addUserComment(int locationId, String userName, String commentContent);
    public ArrayList<DetailsComment> getUserComments(int locationId, int currentPage);
    public ArrayList<Place> GetLovedPlaces(String userName, int index, int maDuLieu);
    public boolean addLovedPlace(String userName, int maDuLieu);
    public boolean deleteLovedPlace(String userName, int maDuLieu);
    public ArrayList<ServiceDetails> getServiceDetails(int maDuLieu);
    public boolean getFeeling(String userName, int maDuLieu);
    public boolean setFeeling(String userName, int maDuLieu, boolean feeling);
}
