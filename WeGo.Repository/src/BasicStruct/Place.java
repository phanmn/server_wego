/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BasicStruct;

/**
 *
 * @author Phan Minh Nhut
 */
public class Place 
{
    public Integer ma_du_lieu;
    public String ten_dich_vu;
    public String ten_dia_diem;
    public String so_nha;
    public String ten_duong;
    public String ten_quan_huyen;
    public String ten_tinh_thanh;
    public float kinh_do;
    public float vi_do;
    public String chu_thich;
    public int so_luot_thich;
    public String ten_phuong;
}
